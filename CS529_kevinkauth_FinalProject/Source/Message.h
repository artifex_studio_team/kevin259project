/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: Message.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "Precompiled.h"
#include "EngineVertex.h"
using namespace std;

namespace Framework
{
	namespace Mid // Message id
	{
		enum MessageIdType
		{
			None,
			Quit,
			Collide,
			GameOver,
			ToggleDebugInfo,
			CharacterKey,
			KeyDown,
			KeyUp,
			MouseButton,
			MouseMove,
			FileDrop,
			RegisterObjectMesh,
			RegisterObjectTexture,
			DrawObject,
			DrawText,
			LogMessage,
			Save,
			Load,
			Continue,
			NewGame
		};
	}

	///Base message class. New message types are defined by deriving from this
	///class and mapping defined an element of the MessageIdType enumeration.
	class Message
	{
	public:
		Message(Mid::MessageIdType id) : MessageId(id){};
		Mid::MessageIdType MessageId;
		virtual ~Message(){};
		virtual std::string GetDescription() 
		{
			return "No description";
		};
	};

	class MessageGameOver : public Message
	{
	public:
		unsigned int Score;
		MessageGameOver(unsigned int score):
			Message(Mid::GameOver)
		{
			this->Score = score;
		}
	};

	class MessageRegisterObjectMesh : public Message
	{
	public:
		vector<SimpleVertex>* VerticesVectorPtr;
		vector<unsigned int>* IndicesVectorPtr;
		D3D10_PRIMITIVE_TOPOLOGY Topology;
		unsigned int* OutObjectMeshIDPtr;
		string TextureFilePath;

		MessageRegisterObjectMesh(
			vector<SimpleVertex>* verticesVectorPtr,
			vector<unsigned int>* indicesVectorPtr,
			D3D10_PRIMITIVE_TOPOLOGY topology,
			string textureFilePath,
			unsigned int* outMeshIDPtr)
			: Message(Mid::RegisterObjectMesh)
		{
			this->VerticesVectorPtr = verticesVectorPtr;
			this->IndicesVectorPtr = indicesVectorPtr;
			this->OutObjectMeshIDPtr = outMeshIDPtr;
			this->Topology = topology;
			this->TextureFilePath = textureFilePath;
		};

		string GetDescription()
		{
			return "Register Object Mesh";
		};
	};

	class MessageDrawObject : public Message
	{
	public:
		unsigned int ObjectMeshID;
		D3DXMATRIX* WorldMatrixPtr;
		D3DXMATRIX* ViewMatrixPtr;
		MessageDrawObject(
			D3DXMATRIX* worldMatrixPtr,
			D3DXMATRIX* viewMatrixPtr,
			unsigned int objectMeshID)
			: Message(Mid::DrawObject)
		{
			this->ObjectMeshID = objectMeshID;
			this->WorldMatrixPtr = worldMatrixPtr;
			this->ViewMatrixPtr = viewMatrixPtr;
		}

		std::string GetDescription()
		{
			return "Draw Object";
		}
	};

	class MessageDrawText : public Message
	{
	public:
		std::string Text;
		RECT LocationRect;
		D3DXVECTOR4 FontColor;
		MessageDrawText(std::string text, RECT locationRect, D3DXVECTOR4 fontColor)
			: Message(Mid::DrawText)
		{
			this->Text = text;
			this->LocationRect = locationRect;
			this->FontColor = fontColor;
		}

		std::string GetDescription()
		{
			return "Draw Text";
		}
	};
}
