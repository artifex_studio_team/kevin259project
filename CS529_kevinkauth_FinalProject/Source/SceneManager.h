/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: SceneManager.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "System.h"
#include "Precompiled.h"
#include "GameObject.h"
#include "GameObjectManager.h"
#include "Scene.h"
#include "SceneCubeFight.h"
#include "SceneMainMenu.h"

using namespace std;

namespace Framework
{
	class SceneManager : public ISystem
	{
	public:

		SceneManager();
		virtual ~SceneManager();

		///Systems can receive all message send to the Core. 
		///See Message.h for details.
		virtual void RecieveMessage(Message* message);

		///All systems are updated every game frame.
		virtual void Update(float timeslice);	

		///All systems provide a string name for debugging.
		virtual std::string GetName() { return "Scene Manager";	}

		///Initialize the system.
		virtual void Initialize();
			
	private:
		Scene* _CurrentScene;
		SceneMainMenu* _SceneMainMenu;
		SceneCubeFight* _SceneCubeFight;
	};
}