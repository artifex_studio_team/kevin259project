/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: LineSegment.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "DirectXIncludes.h"
#include "Precompiled.h"
#include "VectorMath.h"

namespace Framework
{
	class LineSegment
	{
	public:
		D3DXVECTOR3 Pos1;
		D3DXVECTOR3 Pos2;
		D3DXVECTOR3 XZNormal;

		LineSegment();
		void Initialize(D3DXVECTOR3* pos1, D3DXVECTOR3* pos2);
		bool IsPointOnNormalSide(const D3DXVECTOR3* pos);
	};
}