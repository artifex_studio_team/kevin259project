/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: SceneMainMenu.cpp
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "SceneMainMenu.h"

namespace Framework
{
	static const D3DXVECTOR4 UN_HIGHLIGHTED_COLOR = D3DXVECTOR4(0, 256, 0, 256);
	static const D3DXVECTOR4 HIGHLIGHTED_COLOR = D3DXVECTOR4(0, 0, 256, 256);
	static const D3DXVECTOR4 CONTROLS_COLOR = D3DXVECTOR4(256, 0, 0, 256);
	static const D3DXVECTOR4 TITLE_COLOR = D3DXVECTOR4(125, 125, 125, 256);
	static float MessageFreezeTimer = 1.0f;

	SceneMainMenu::SceneMainMenu()
	{
		int firstTop = 50;
		int deltaHeight = 40;
		int width = 500;
		int height = 30;
		int left = 5;

		// Create new game message
		RECT location;
		location.bottom = firstTop + height;
		location.top = firstTop;
		location.left = left;
		location.right = left + width;

		this->_OptionsTitle = new MessageDrawText("Options (Click):", location, TITLE_COLOR);

		location.bottom += deltaHeight;
		location.top += deltaHeight;
		this->_NewGameMessage = new MessageDrawText("New Game", location, UN_HIGHLIGHTED_COLOR);

		// Create continue game message
		location.bottom += deltaHeight;
		location.top += deltaHeight;
		this->_ContinueGameMessage = new MessageDrawText("Continue Game", location, UN_HIGHLIGHTED_COLOR);

		location.bottom += deltaHeight;
		location.top += deltaHeight;
		this->_LoadGameMessage = new MessageDrawText("Load Last Save", location, UN_HIGHLIGHTED_COLOR);

		// Create quit message
		location.bottom += deltaHeight;
		location.top += deltaHeight;
		this->_QuitMessage = new MessageDrawText("Save and Quit", location, UN_HIGHLIGHTED_COLOR);

		// Controls title
		location.bottom += deltaHeight;
		location.top += deltaHeight;
		this->_ControlsTitle = new MessageDrawText("Contols:", location, TITLE_COLOR);

		// Forward control
		location.bottom += deltaHeight;
		location.top += deltaHeight;
		this->_ForwardControl = new MessageDrawText("Foreward: W", location, CONTROLS_COLOR);

		// Left control
		location.bottom += deltaHeight;
		location.top += deltaHeight;
		this->_LeftControl = new MessageDrawText("Left: A", location, CONTROLS_COLOR);

		// Right control
		location.bottom += deltaHeight;
		location.top += deltaHeight;
		this->_RightControl = new MessageDrawText("Right: D", location, CONTROLS_COLOR);
		
		// Brake control
		location.bottom += deltaHeight;
		location.top += deltaHeight;
		this->_BrakeControl = new MessageDrawText("Brake: S", location, CONTROLS_COLOR);
		
		// Right control
		location.bottom += deltaHeight;
		location.top += deltaHeight;
		this->_BulletControl = new MessageDrawText("Fire Bullet: Space", location, CONTROLS_COLOR);

		// Right control
		location.bottom += deltaHeight;
		location.top += deltaHeight;
		this->_MissileControl = new MessageDrawText("Fire Homing Missile: M", location, CONTROLS_COLOR);

		// Right control
		location.bottom += deltaHeight;
		location.top += deltaHeight;
		this->_MainMenuControl = new MessageDrawText("Main Menu: esc", location, CONTROLS_COLOR);
	}

	SceneMainMenu::~SceneMainMenu()
	{
		if (this->_NewGameMessage)
		{
			delete this->_NewGameMessage;
		}

		if (this->_ContinueGameMessage)
		{
			delete this->_ContinueGameMessage;
		}

		if (this->_LoadGameMessage)
		{
			delete this->_LoadGameMessage;
		}

		if (this->_QuitMessage)
		{
			delete this->_QuitMessage;
		}

		if (this->_BrakeControl)
		{
			delete this->_BrakeControl;
		}

		if (this->_BulletControl)
		{
			delete this->_BulletControl;
		}

		if (this->_ForwardControl)
		{
			delete this->_ForwardControl;
		}

		if (this->_LeftControl)
		{
			delete this->_LeftControl;
		}

		if (this->_RightControl)
		{
			delete this->_RightControl;
		}

		if (this->_MissileControl)
		{
			delete this->_MissileControl;
		}

		if (this->_MainMenuControl)
		{
			delete this->_MainMenuControl;
		}
	}

	void SceneMainMenu::SceneLoad()
	{
		// Nothing right now
	}

	void SceneMainMenu::SceneInit()
	{
		// Nothing right now
	}

	void SceneMainMenu::SceneUpdate(float timeSlice)
	{
		// Logic for effects and stuff goes here
		if (MessageFreezeTimer > 0)
		{
			MessageFreezeTimer -= timeSlice;
		}
	}

	void SceneMainMenu::SceneDraw()
	{
		// Draw each message
		SYSTEM_MANAGER->BroadcastMessage(this->_OptionsTitle);
		SYSTEM_MANAGER->BroadcastMessage(this->_NewGameMessage);
		SYSTEM_MANAGER->BroadcastMessage(this->_ContinueGameMessage);
		SYSTEM_MANAGER->BroadcastMessage(this->_LoadGameMessage);
		SYSTEM_MANAGER->BroadcastMessage(this->_QuitMessage);
		SYSTEM_MANAGER->BroadcastMessage(this->_ControlsTitle);
		SYSTEM_MANAGER->BroadcastMessage(this->_ForwardControl);
		SYSTEM_MANAGER->BroadcastMessage(this->_LeftControl);
		SYSTEM_MANAGER->BroadcastMessage(this->_RightControl);
		SYSTEM_MANAGER->BroadcastMessage(this->_BrakeControl);
		SYSTEM_MANAGER->BroadcastMessage(this->_BulletControl);
		SYSTEM_MANAGER->BroadcastMessage(this->_MissileControl);
		SYSTEM_MANAGER->BroadcastMessage(this->_MainMenuControl);
	}

	void SceneMainMenu::SceneFree()
	{
	}

	void SceneMainMenu::SceneUnload()
	{
	}

	void SceneMainMenu::RecieveMessage(Message* message)
	{	
		// Check for key press messages
		MessageKeyDown* messageKeyDownPtr = dynamic_cast<MessageKeyDown*>(message);

		// Check for esc key
		if (messageKeyDownPtr)
		{
			// Esc key
			if (messageKeyDownPtr->Character == 27)
			{
				// Freeze the next few messages
				MessageFreezeTimer = 1.0f;
			}
		}

		// Cause the freeze
		if (MessageFreezeTimer > 0)
		{
			return;
		}

		// Check for cursor move message
		MouseMove* mouseMovePtr = dynamic_cast<MouseMove*>(message);
		if (mouseMovePtr)
		{
			this->_NewGameMessage->FontColor = UN_HIGHLIGHTED_COLOR;
			this->_ContinueGameMessage->FontColor = UN_HIGHLIGHTED_COLOR;
			this->_LoadGameMessage->FontColor = UN_HIGHLIGHTED_COLOR;
			this->_QuitMessage->FontColor = UN_HIGHLIGHTED_COLOR;
			if (this->_IsCursorOverMenuItem(this->_NewGameMessage, mouseMovePtr->MousePosition))
			{
				this->_NewGameMessage->FontColor = HIGHLIGHTED_COLOR;
			}
			else if (this->_IsCursorOverMenuItem(this->_ContinueGameMessage, mouseMovePtr->MousePosition))
			{
				this->_ContinueGameMessage->FontColor = HIGHLIGHTED_COLOR;
			}
			else if (this->_IsCursorOverMenuItem(this->_LoadGameMessage, mouseMovePtr->MousePosition))
			{
				this->_LoadGameMessage->FontColor = HIGHLIGHTED_COLOR;
			}
			else if (this->_IsCursorOverMenuItem(this->_QuitMessage, mouseMovePtr->MousePosition))
			{
				this->_QuitMessage->FontColor = HIGHLIGHTED_COLOR;
			}
		}

		// Check for click message
		MouseButton* mouseButtonPtr = dynamic_cast<MouseButton*>(message);
		if (mouseButtonPtr)
		{
			// If left click
			if (mouseButtonPtr->MouseButtonIndex == mouseButtonPtr->LeftMouse)
			{
				if (this->_IsCursorOverMenuItem(this->_NewGameMessage, mouseButtonPtr->MousePosition))
				{
					// New game clicked
					Message message(Mid::NewGame);
					SYSTEM_MANAGER->BroadcastMessage(&message);
				}
				else if (this->_IsCursorOverMenuItem(this->_LoadGameMessage, mouseButtonPtr->MousePosition))
				{
					SYSTEM_MANAGER->BroadcastMessage(&Message(Mid::Load));
				}
				else if (this->_IsCursorOverMenuItem(this->_ContinueGameMessage, mouseButtonPtr->MousePosition))
				{
					// Continue game clicked
					SYSTEM_MANAGER->BroadcastMessage(&Message(Mid::Continue));
				}
				else if (this->_IsCursorOverMenuItem(this->_QuitMessage, mouseButtonPtr->MousePosition))
				{
					// Save the game
					SYSTEM_MANAGER->BroadcastMessage(&Message(Mid::Save));

					// Quit the game
					SYSTEM_MANAGER->BroadcastMessage(&Message(Mid::Quit));
				}
			}
		}
	}

	bool SceneMainMenu::_IsCursorOverMenuItem(MessageDrawText* menuItem, D3DXVECTOR2 position)
	{
		RECT itemRect = menuItem->LocationRect;
		if (position.x > itemRect.left &&
			position.x < itemRect.right &&
			position.y < itemRect.bottom &&
			position.y > itemRect.top)
		{
			return true;
		}

		return false;
	}
}