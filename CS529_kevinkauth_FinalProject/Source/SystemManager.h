/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: SystemManager.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once //Makes sure this header is only included once

#include "System.h"

namespace Framework
{
	///The core manages all the systems in the game. Updating them, routing messages, and
	///destroying them when the game ends.
	class SystemManager
	{
	public:
		SystemManager();
		~SystemManager();

		///Update all the systems until the game is no longer active.
		void GameLoop();
		///Destroy all systems in reverse order that they were added.
		void DestroySystemsAndQuit();

		///Broadcasts a message to all systems.
		void BroadcastMessage(Message* m);
		///Adds a new system to the game.
		void AddSystem(ISystem* system);
		///Initializes all systems in the game.
		void Initialize();
	private:
		//Tracks all the systems the game uses
		std::vector<ISystem*> Systems;
		//The last time the game was updated
		unsigned LastTime;
		//Is the game running (true) or being shut down (false)?
		bool GameActive;
	};

	///Message to tell the game to quit
	class MessageQuit : public Message
	{
	public:
		MessageQuit() : Message(Mid::Quit) {};
		std::string GetDescription()
		{
			return "Quit";
		}
	};

	///Signals all systems to activate or deactivate the display of debug data.
	class ToggleDebugDisplay : public Message
	{
	public:
		bool DebugActive;
		ToggleDebugDisplay(bool debugActive)
			: Message(Mid::ToggleDebugInfo) , DebugActive(debugActive) {};
	};

	//A global pointer to the instance of the core
	extern SystemManager* SYSTEM_MANAGER;
}