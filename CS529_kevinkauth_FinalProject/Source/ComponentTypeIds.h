/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: ComponentTypeIds.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once

namespace Framework
{
	//A simple enumeration used to identify components quickly and efficiently.
	//There is alternative methods for generating ids automatically, 
	//(string hashing, static type ids).
	//But this is a clean, easy, understandable and stable way.
	enum ComponentTypeId
	{
		//GraphicsManager
		CT_3DDrawing,
		CT_Physics,
	};

}