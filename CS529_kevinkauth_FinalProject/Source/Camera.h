/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: Camera.h
Purpose: Header for camera objects
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once;
#include "Precompiled.h"
#include "DirectXIncludes.h"
#include "VectorMath.h"

namespace Framework
{
	class Camera
	{
	public:
		float DistanceFromFocusPoint;
		float CameraHeight;
		D3DXVECTOR3 Position;
		D3DXVECTOR3 FocusPoint;
		D3DXVECTOR3 UpVector;
		float XZPlaneAngle;

		Camera(
			D3DXVECTOR3 focusPoint = D3DXVECTOR3(0.0f, 0.0f, 0.0f),
			D3DXVECTOR3 position = D3DXVECTOR3(0.0f, 5.0f, 5.0f),
			float xzPlaneAngle = 0.0f,
			float cameraHeight = 5.0f,
			float distanceFromFocusPoint = 5.0f,
			D3DXVECTOR3 upVector = D3DXVECTOR3(0.0f, 1.0f, 0.0f));
		virtual ~Camera(){};
		D3DXMATRIX* GetViewMatrix();

	private:
		D3DXMATRIX _ViewMatrix;
		void _UpdatePosition();
	};
}