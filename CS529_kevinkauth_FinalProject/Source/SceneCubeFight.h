/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: SceneCubeFight.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "Scene.h"
#include "PhysicsComponent.h"
#include <iostream>
#include <fstream>

namespace Framework
{
	class SceneCubeFight : public Scene
	{
	public:
		SceneCubeFight(bool loadFromFile);
		~SceneCubeFight();

		virtual void SceneLoad();
		virtual void SceneInit();
		virtual void SceneUpdate(float timeSlice);
		virtual void SceneDraw();
		virtual void SceneFree();
		virtual void SceneUnload();
		virtual void RecieveMessage(Message* message);
		void SaveGame();

	private:
		unsigned int _LifeCount;
		unsigned int _Score;
		GameObject* _FloorObjectPtr;
		GameObject* _HeroCubeObjectPtr;
		MessageDrawText* _ScoreText;
		MessageDrawText* _LivesText;

		void _RegisterCubeMesh(D3DXVECTOR4 cubeColor, string textureFilePath, unsigned int* outMeshID);
		void _CollisionCheck(GameObject* obj);
		void _RefreshText();
	};
}