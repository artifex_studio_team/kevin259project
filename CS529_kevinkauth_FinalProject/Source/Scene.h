/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: Scene.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "Precompiled.h"
#include "SystemManager.h"
#include "Message.h"
#include "GameObjectManager.h"
#include "GraphicsManager.h"
#include "DrawingComponent.h"

namespace Framework
{
	class Scene
	{
	public:
		virtual void SceneLoad() = 0;
		virtual void SceneInit() = 0;
		virtual void SceneUpdate(float timeSlice) = 0;
		virtual void SceneDraw() = 0;
		virtual void SceneFree() = 0;
		virtual void SceneUnload() = 0;
		virtual void RecieveMessage(Message* message) = 0;
	protected:
		Camera* SceneCameraPtr;
		GameObjectManager* ObjectManagerPtr;
	};
}