/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: LoggingManager.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "Precompiled.h"
#include "System.h"
#include "Message.h"
#include <iostream>
#include <fstream>
#include "Message.h"

using namespace std;

/*
ios::app   -- Append to the file
ios::ate   -- Set the current position to the end
ios::trunc -- Delete everything in the file
*/

namespace Framework
{
	class LoggingManager : public ISystem
	{
	public:
		virtual void RecieveMessage(Message* message);

		virtual void Update(float timeslice) {};

		///All systems provide a string name for debugging.
		virtual std::string GetName() { return "Logging Manager"; };

		///Initialize the system.
		virtual void Initialize();
		
		///All systems need a virtual destructor to have their destructor called 
		virtual ~LoggingManager(){};	
	private:
		ofstream logFile;
	};

	class LoggingMessage : public Message
	{
	public:
		std::string LogMessage;
		LoggingMessage(std::string logMessage) : Message(Mid::LogMessage) 
		{
			this->LogMessage = logMessage;
		};
		std::string GetDescription()
		{
			return "Log Message" + this->LogMessage;
		}
	};
}