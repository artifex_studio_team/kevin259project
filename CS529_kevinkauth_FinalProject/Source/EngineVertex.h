/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: EngineVertex.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "DirectXIncludes.h"

namespace Framework
{
	struct SimpleVertex
	{
		SimpleVertex(D3DXVECTOR4 pos, D3DXVECTOR2 textureCoord) : Pos(pos), TextureCoord(textureCoord) {};
		D3DXVECTOR4 Pos;
		D3DXVECTOR2 TextureCoord;
	};
}