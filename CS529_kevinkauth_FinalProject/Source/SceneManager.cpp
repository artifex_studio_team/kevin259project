/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: SceneManager.cpp
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "SceneManager.h"
#include "SceneCubeFight.h"
#include "SceneMainMenu.h"

namespace Framework
{
	SceneManager::SceneManager()
	{
		this->_SceneCubeFight = new SceneCubeFight(false);
		this->_SceneMainMenu = new SceneMainMenu();
		this->_CurrentScene = 0;
	}

	SceneManager::~SceneManager()
	{
		if (this->_SceneCubeFight)
		{
			delete this->_SceneCubeFight;
			this->_SceneCubeFight = 0;
		}

		if (this->_SceneMainMenu)
		{
			delete this->_SceneMainMenu;
			this->_SceneMainMenu = 0;
		}
	}

	void SceneManager::Initialize()
	{
		this->_SceneMainMenu->SceneLoad();
		this->_SceneMainMenu->SceneInit();
		this->_SceneCubeFight->SceneLoad();
		this->_SceneCubeFight->SceneInit();

		this->_CurrentScene = this->_SceneMainMenu;
	}

	void SceneManager::Update(float timeSlice)
	{
		this->_CurrentScene->SceneUpdate(timeSlice);
		this->_CurrentScene->SceneDraw();
	}

	void SceneManager::RecieveMessage(Message* message)
	{
		// Check for key press messages
		MessageKeyDown* messageKeyDownPtr = dynamic_cast<MessageKeyDown*>(message);

		MessageGameOver* gameOverMessagePtr = dynamic_cast<MessageGameOver*>(message);
		if (gameOverMessagePtr)
		{
			// Reset the current cube fight scene with a fresh game
			if (this->_SceneCubeFight)
			{
				delete this->_SceneCubeFight;
				this->_SceneCubeFight = 0;
			}

			this->_SceneCubeFight = new SceneCubeFight(false);
			this->_SceneCubeFight->SceneLoad();
			this->_SceneCubeFight->SceneInit();
		}

		if (messageKeyDownPtr)
		{
			// Esc key
			if (messageKeyDownPtr->Character == 27)
			{
				// Switch to main menu
				this->_CurrentScene = this->_SceneMainMenu;
			}
		}		
		else if (message->MessageId == Mid::NewGame)
		{	
			this->_SceneCubeFight->SceneFree();
			this->_SceneCubeFight->SceneInit();
			this->_CurrentScene = this->_SceneCubeFight;
		}
		else if (gameOverMessagePtr)
		{
			// Switch to main menu
			this->_CurrentScene = this->_SceneMainMenu;
			// Set the current cube fight scene to a fresh game
			
			if (this->_SceneCubeFight)
			{
				delete this->_SceneCubeFight;
				this->_SceneCubeFight = 0;
			}

			this->_SceneCubeFight = new SceneCubeFight(false);
			this->_SceneCubeFight->SceneLoad();
			this->_SceneCubeFight->SceneInit();
		}
		else if (message->MessageId == Mid::Save)
		{
			if (this->_SceneCubeFight)
			{
				this->_SceneCubeFight->SaveGame();
			}
		
		}
		else if (message->MessageId == Mid::Continue)
		{
			if (this->_SceneCubeFight)
			{
				this->_CurrentScene = this->_SceneCubeFight;
			}
		}
		else if (message->MessageId == Mid::Load)
		{
			if (this->_SceneCubeFight)
			{
				delete this->_SceneCubeFight;
				this->_SceneCubeFight = 0;
			}

			this->_SceneCubeFight = new SceneCubeFight(true);
			this->_CurrentScene = this->_SceneCubeFight;
		}

		// Pass to current scene
		if (this->_CurrentScene)
		{
				
			this->_CurrentScene->RecieveMessage(message);
		}
	}
}