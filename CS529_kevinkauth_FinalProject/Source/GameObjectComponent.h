/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: GameObjectComponent.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once //Makes sure this header is only included once

#include "Message.h"
#include "ComponentTypeIds.h"

namespace Framework
{
	//Forward declaration of GOC class
	class GameObject;
	typedef GameObject GOC;
	
	///A component is an atomic piece of functionality that is
	///composed onto a GameObject to form game objects.
	class GameObjectComponent 
	{
		friend class GameObject;
	public:
		
		///Signal that the component is now active in the game world.
		///See GameObject for details.
		virtual void Initialize(){};

		///GameComponents receive all messages send to their owning composition. 
		///See Message.h for details.
		virtual void RecieveMessage(Message *){};

		///Get the GameObject this component is owned/composed.
		GOC* GetOwner(){ return Base; }

		ComponentTypeId TypeId;

	protected:
		///Destroy the component.
		virtual ~GameObjectComponent(){};
	private:
		///Each component has a pointer back to the base owning composition.
		GOC * Base;
	};
}
