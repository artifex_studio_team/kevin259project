/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: Camera.cpp
Purpose: Code for camera objects
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "Camera.h"

namespace Framework
{
	Camera::Camera(
			D3DXVECTOR3 focusPoint,
			D3DXVECTOR3 position,
			float xzPlaneAngle,
			float cameraHeight,
			float distanceFromFocusPoint,
			D3DXVECTOR3 upVector)
	{
		this->FocusPoint = focusPoint;
		this->Position = position;
		this->XZPlaneAngle = xzPlaneAngle;
		this->CameraHeight = cameraHeight;
		this->DistanceFromFocusPoint = distanceFromFocusPoint;
		this->UpVector = upVector;
	}

	D3DXMATRIX* Camera::GetViewMatrix()
	{
		this->_UpdatePosition();

		D3DXMatrixLookAtLH(
			&this->_ViewMatrix,
			&this->Position,
			&this->FocusPoint,
			&this->UpVector);
		return &this->_ViewMatrix;
	}

	void Camera::_UpdatePosition()
	{
		float pi =  3.1415926535897f;
			
		// Create a position in the positive x direction of length 1
		this->Position = D3DXVECTOR3(1.0f, 0.0f, 0.0f);

		// Rotate around Y axis by xz plane angle
		this->Position.x = cos(this->XZPlaneAngle + pi);
		this->Position.z = sin(this->XZPlaneAngle + pi);

		float distance = VectorMath::DistanceBetweenPoints(&this->Position, &this->FocusPoint);
		
		// Scale to desired distance
		VectorMath::ScaleVector(&this->Position, &this->Position, this->DistanceFromFocusPoint);

		// Translate to world location
		this->Position += this->FocusPoint;

		// Set the final height
		this->Position.y = this->CameraHeight;
	}
}