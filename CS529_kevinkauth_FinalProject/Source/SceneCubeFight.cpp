/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: SceneCubeFight.cpp
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "SceneCubeFight.h"

namespace Framework
{
	static const string SAVE_FILE_PATH = "GameSave.txt";
	static const string FLOOR_TEXTURE_FILE_PATH = "FloorTexture.png";
	static const string HERO_TEXTURE_FILE_PATH = "HeroTexture.png";
	static const string HOMING_ENEMY_TEXTURE_FILE_PATH = "HomingEnemyTexture.png";
	static const string TURRET_ENEMY_TEXTURE_FILE_PATH = "TurretEnemyTexture.png";
	static const string BULLET_TEXTURE_FILE_PATH = "BulletTexture.png";
	static const string HOMING_MISSILE_TEXTURE_FILE_PATH = "MissileTexture.png";
	static const float ARENA_SIZE = 100.0f;
	static const float BULLET_SPEED = 50.0f;
	static const float MISSILE_SPEED = 50.0f;
	static const float BULLET_SCALE = 0.5f;
	static const float MISSILE_SCALE = 0.75f;
	static const float TURRET_ENEMY_SCALE = 3.0f;
	static const float HERO_SCALE = 1.0f;
	static const float HOMING_ENEMY_SCALE = 1.0f;
	static const float MOUSE_SENSITIVITY = 0.1f;
	static const float BULLET_FIRE_FREQUENCY = 5.0f;
	static const D3DXVECTOR4 TEXT_COLOR = D3DXVECTOR4(256, 256, 256, 256); // White, full alpha
	static bool MESH_REGISTERED = false;
	static unsigned int HeroCubeMeshID;
	static unsigned int HomingEnemyCubeMeshID;
	static unsigned int TurretEnemyCubeMeshID;
	static unsigned int BulletCubeMeshID;
	static unsigned int MissileCubeMeshID;
	static unsigned int FloorMeshID;
	static D3DXVECTOR2 LastMousePos;
	static float BulletTimeCounter = 0.0f;

	/*
		Helper function from 
		http://www.cplusplus.com/forum/beginner/7777/
	*/
	string convertInt(unsigned int number)
	{
		if (number == 0)
			return "0";
		string temp="";
		string returnvalue="";
		while (number>0)
		{
			temp+=number%10+48;
			number/=10;
		}
		for (unsigned int i=0;i<temp.length();i++)
			returnvalue+=temp[temp.length()-i-1];
		return returnvalue;
	}

	SceneCubeFight::~SceneCubeFight()
	{
		if (this->_ScoreText)
		{
			delete this->_ScoreText;
			this->_ScoreText = 0;
		}
		if (this->_LivesText)
		{
			delete this->_LivesText;
			this->_LivesText = 0;
		}
		if (this->ObjectManagerPtr)
		{
			delete this->ObjectManagerPtr;
			this->ObjectManagerPtr = 0;
		}
		if (this->SceneCameraPtr)
		{
			delete this->SceneCameraPtr;
			this->SceneCameraPtr = 0;
		}
	}

	SceneCubeFight::SceneCubeFight(bool loadFromFile)
	{
		// Initialize pointers to null
		this->_FloorObjectPtr = 0;
		this->_HeroCubeObjectPtr = 0;
		this->_ScoreText = 0;
		this->_LivesText = 0;
		this->ObjectManagerPtr = 0;
		this->SceneCameraPtr = 0;

		// Try to open save file
		ifstream reader;
		reader.open(SAVE_FILE_PATH);

		// Check if we are loading from the save file
		if (loadFromFile && reader.good())
		{
			this->SceneLoad();

			// Read from file and change appropriate properties
			
			string nextLine;

			getline(reader, nextLine);
			this->_Score = atoi(nextLine.c_str());

			getline(reader, nextLine);
			this->_LifeCount = atoi(nextLine.c_str());

			this->ObjectManagerPtr = new GameObjectManager(&reader);

			// Assume floor is first
			this->_FloorObjectPtr = this->ObjectManagerPtr->ObjectArray[0];

			// Assume hero is second
			this->_HeroCubeObjectPtr = this->ObjectManagerPtr->ObjectArray[1];

			// Create initial camera
			this->SceneCameraPtr = new Camera();
			this->SceneCameraPtr->FocusPoint = this->_HeroCubeObjectPtr->Position;
			this->SceneCameraPtr->XZPlaneAngle = *this->_HeroCubeObjectPtr->GetRotationY() + 3.1415926535897f;
		}
		else if (loadFromFile)
		{
			// Loading the file failed unexpectedly, we will need to initialze as new game aswell
			this->ObjectManagerPtr = new GameObjectManager();
			this->SceneInit();
		}
		else
		{
			this->ObjectManagerPtr = new GameObjectManager();
		}
	}

	void SceneCubeFight::SaveGame()
	{
		// Open output stream, delete previous content
		ofstream writer;
		writer.open(SAVE_FILE_PATH, ios::trunc);

		// Write metrics
		writer << this->_Score << '\n';
		writer << this->_LifeCount << '\n';

		// Write objects
		writer << this->ObjectManagerPtr->ToString();
	}

	void SceneCubeFight::SceneLoad()
	{
		this->_ScoreText = 0;
		this->_LivesText = 0;
		this->SceneCameraPtr = 0;

		// Create draw text message objects for score and lives
		this->_Score = 0;
		this->_LifeCount = 3;
		this->_RefreshText();

		if (!MESH_REGISTERED)
		{
			// Register mesh's for sube objects
			this->_RegisterCubeMesh(D3DXVECTOR4(0.0f, 0.0f, 1.0f, 1.0f), HERO_TEXTURE_FILE_PATH, &HeroCubeMeshID);
			this->_RegisterCubeMesh(D3DXVECTOR4(0.0f, 1.0f, 0.0f, 1.0f), BULLET_TEXTURE_FILE_PATH, &BulletCubeMeshID);
			this->_RegisterCubeMesh(D3DXVECTOR4(0.0f, 1.0f, 0.0f, 1.0f), HOMING_MISSILE_TEXTURE_FILE_PATH, &MissileCubeMeshID);
			this->_RegisterCubeMesh(D3DXVECTOR4(1.0f, 0.0f, 0.0f, 1.0f), HOMING_ENEMY_TEXTURE_FILE_PATH, &HomingEnemyCubeMeshID);
			this->_RegisterCubeMesh(D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f), TURRET_ENEMY_TEXTURE_FILE_PATH, &TurretEnemyCubeMeshID);

			// Register mesh for floor
			std::vector<SimpleVertex> floorVertexVector;
			floorVertexVector.push_back(SimpleVertex(D3DXVECTOR4(-ARENA_SIZE, 0.0f, -ARENA_SIZE, 1.0f), D3DXVECTOR2(0.0f, 0.0f)));
			floorVertexVector.push_back(SimpleVertex(D3DXVECTOR4(ARENA_SIZE, 0.0f, ARENA_SIZE, 1.0f), D3DXVECTOR2(1.0f, 0.0f)));
			floorVertexVector.push_back(SimpleVertex(D3DXVECTOR4(ARENA_SIZE, 0.0f, -ARENA_SIZE, 1.0f), D3DXVECTOR2(1.0f, 1.0f)));
			floorVertexVector.push_back(SimpleVertex(D3DXVECTOR4(-ARENA_SIZE, 0.0f, ARENA_SIZE, 1.0f), D3DXVECTOR2(0.0f, 1.0f)));

			std::vector<unsigned int> floorIndices;
			floorIndices.push_back(0), floorIndices.push_back(1), floorIndices.push_back(2);
			floorIndices.push_back(3), floorIndices.push_back(1), floorIndices.push_back(0);

			SYSTEM_MANAGER->BroadcastMessage(
				&MessageRegisterObjectMesh(
					&floorVertexVector,
					&floorIndices,
					D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
					FLOOR_TEXTURE_FILE_PATH,
					&FloorMeshID));

			MESH_REGISTERED = true;
		}
	}

	void SceneCubeFight::SceneInit()
	{
		// Reset metrics
		this->_Score = 0;
		this->_LifeCount = 3;

		// Create floor
		this->ObjectManagerPtr->CreateObject(FloorMeshID, true);

		// Create hero cube, record pointer to it
		this->_HeroCubeObjectPtr = this->ObjectManagerPtr->CreateObject(HeroCubeMeshID, false);
		this->_HeroCubeObjectPtr->Position = D3DXVECTOR3(0.0f, HERO_SCALE / 2.0f, 0.0f);
		this->_HeroCubeObjectPtr->Scale = HERO_SCALE;

		// Create initial camera
		this->SceneCameraPtr = new Camera();
		this->SceneCameraPtr->FocusPoint = this->_HeroCubeObjectPtr->Position;
		this->SceneCameraPtr->XZPlaneAngle = *this->_HeroCubeObjectPtr->GetRotationY() + 3.1415926535897f;

		GameObject* turretEnemy = this->ObjectManagerPtr->CreateObject(TurretEnemyCubeMeshID, true);
		turretEnemy->Scale = TURRET_ENEMY_SCALE;
		turretEnemy->Position = D3DXVECTOR3(ARENA_SIZE / 2.0f, TURRET_ENEMY_SCALE / 2.0f, ARENA_SIZE / 2.0f);

		turretEnemy = this->ObjectManagerPtr->CreateObject(TurretEnemyCubeMeshID, true);
		turretEnemy->Scale = TURRET_ENEMY_SCALE;
		turretEnemy->Position = D3DXVECTOR3(ARENA_SIZE / 2.0f, TURRET_ENEMY_SCALE / 2.0f, -ARENA_SIZE / 2.0f);

		turretEnemy = this->ObjectManagerPtr->CreateObject(TurretEnemyCubeMeshID, true);
		turretEnemy->Scale = TURRET_ENEMY_SCALE;
		turretEnemy->Position = D3DXVECTOR3(-ARENA_SIZE / 2.0f, TURRET_ENEMY_SCALE / 2.0f, ARENA_SIZE / 2.0f);

		turretEnemy = this->ObjectManagerPtr->CreateObject(TurretEnemyCubeMeshID, true);
		turretEnemy->Scale = TURRET_ENEMY_SCALE;
		turretEnemy->Position = D3DXVECTOR3(-ARENA_SIZE / 2.0f, TURRET_ENEMY_SCALE / 2.0f, -ARENA_SIZE / 2.0f);

		// Create enemies
		for (int x = 1; x < 15; ++x)
		{
			// Homing enemy creation
			GameObject* enemy = this->ObjectManagerPtr->CreateObject(HomingEnemyCubeMeshID, false);

			// Odd numbers
			if (x % 2)
			{
				enemy->Position = D3DXVECTOR3((float)x, HOMING_ENEMY_SCALE / 2.0f, (float)x * 2.0f);
			}
			// Even numbers
			else
			{	
				enemy->Position = D3DXVECTOR3((float)x, HOMING_ENEMY_SCALE / 2.0f, (float)-x * 2.0f);
			}
		}
	}

	void SceneCubeFight::SceneUpdate(float timeSlice)
	{
		BulletTimeCounter += timeSlice;

		// For each object
		for (unsigned int i = 0; i <= this->ObjectManagerPtr->HighestLiveIndex; ++i)
		{
			// Get the next object
			GameObject* nextObject1 = this->ObjectManagerPtr->ObjectArray[i];
			
			// Skip if dead
			if (nextObject1->LifeState == DEAD)
			{
				continue;
			}

			// Check if colliding against all other objects
			this->_CollisionCheck(nextObject1);
		}

		// Update object positions
		for (unsigned int i = 0; i <= this->ObjectManagerPtr->HighestLiveIndex; ++i)
		{
			GameObject* nextObject = this->ObjectManagerPtr->ObjectArray[i];
			
			// Skip if dead or floor
			if (nextObject->LifeState == DEAD || nextObject->Type == FloorMeshID)
			{
				continue;
			}

			// If homing enemy, home in on hero
			if (nextObject->Type == HomingEnemyCubeMeshID)
			{
				PhysicsComponent::HomeObjectOnTarget(nextObject, this->_HeroCubeObjectPtr);
			}
			// If turret enemy and bullet fire time, fire bullet at hero
			else if (nextObject->Type == TurretEnemyCubeMeshID && BulletTimeCounter > BULLET_FIRE_FREQUENCY)
			{
				D3DXVECTOR3 vecToHero = this->_HeroCubeObjectPtr->Position - nextObject->Position;
				GameObject* bullet = this->ObjectManagerPtr->CreateObject(BulletCubeMeshID, false);
				bullet->Scale = BULLET_SCALE;
				bullet->Position = nextObject->Position;

				// Position bullet
				VectorMath::Normalize(&vecToHero, &vecToHero);
				VectorMath::ScaleVector(&vecToHero, &vecToHero, TURRET_ENEMY_SCALE * 2.0f);
				bullet->Position += vecToHero;

				// Set velocity
				VectorMath::Normalize(&vecToHero, &vecToHero);
				VectorMath::ScaleVector(&vecToHero, &vecToHero, BULLET_SPEED);
				bullet->SetVelocity(vecToHero);
			}

			// If homing missile find and home in on nearest enemy
			else if (nextObject->Type == MissileCubeMeshID)
			{
				float shortestDistance = 100000.0;
				GameObject* bestEnemy = 0;

				// For each object
				for (unsigned int i = 0; i <= this->ObjectManagerPtr->HighestLiveIndex; ++i)
				{
					GameObject* nextObject2 = this->ObjectManagerPtr->ObjectArray[i];

					// Get next object, skip if not enemy, skip if dead
					if (nextObject2->LifeState == DEAD || nextObject2->Type != HomingEnemyCubeMeshID)
					{
						continue;
					}

					// Get distance between missile and this enemy
					float distance = VectorMath::DistanceBetweenPoints(&nextObject->Position, &nextObject2->Position);

					if (distance < shortestDistance)
					{
						bestEnemy = nextObject2;
						shortestDistance = distance;
					}
				}

				// If not null, home in on that enemy
				if (bestEnemy)
				{
					PhysicsComponent::HomeObjectOnTarget(nextObject, bestEnemy);
				}
			}


			nextObject->UpdatePosition(timeSlice);

			// If they are past the boundaries of the arena, wrap them to the other side or destroy them
			if (nextObject->Position.x > ARENA_SIZE ||
				nextObject->Position.x < -ARENA_SIZE ||
				nextObject->Position.z > ARENA_SIZE ||
				nextObject->Position.z < -ARENA_SIZE)
			{
				// Object is out of bounds
				// If hero
				if (nextObject->Type == HeroCubeMeshID)
				{
					// Decrement lives count
					this->_LifeCount--;

					// If we are <= 0  game over
					if (this->_LifeCount <= 0)
					{
						// Game Over
						SYSTEM_MANAGER->BroadcastMessage(&MessageGameOver(this->_Score));
						return;
					}
					
					// Reset at origin
					this->_HeroCubeObjectPtr->Position = D3DXVECTOR3(0.0f, HERO_SCALE / 2.0f, 0.0f);
					this->_HeroCubeObjectPtr->SetVelocity(D3DXVECTOR3(0.0f, 0.0f, 0.0f));
				}
				// If homing enemy or turret enemy
				else if (nextObject->Type == HomingEnemyCubeMeshID || nextObject->Type == TurretEnemyCubeMeshID)
				{
					// Increment score
					this->_Score++;

					// Destroy it
					nextObject->Destroy();
				}
				// If bullet or missile
				else if (nextObject->Type == BulletCubeMeshID || nextObject->Type == MissileCubeMeshID)
				{
					// Destroy bullet
					nextObject->Destroy();
				}
			}
		}

		// Update the camera
		this->SceneCameraPtr->FocusPoint = this->_HeroCubeObjectPtr->Position;

		if (BulletTimeCounter > BULLET_FIRE_FREQUENCY)
		{
			BulletTimeCounter -= BULLET_FIRE_FREQUENCY;
		}
	}

	void SceneCubeFight::SceneDraw()
	{
		// Draw objects
		this->ObjectManagerPtr->DrawAllLiveObjects(this->SceneCameraPtr);

		// Draw text
		this->_RefreshText();
		SYSTEM_MANAGER->BroadcastMessage(this->_ScoreText);
		SYSTEM_MANAGER->BroadcastMessage(this->_LivesText);
	}

	void SceneCubeFight::SceneFree()
	{
		if (this->ObjectManagerPtr)
		{
			// Destroy all objects
			this->ObjectManagerPtr->DestroyAllObjects();
		}

		// Destroy the camera
		if (this->SceneCameraPtr)
		{
			delete this->SceneCameraPtr;
			this->SceneCameraPtr = 0;
		}
	}

	void SceneCubeFight::SceneUnload()
	{
	}

	void SceneCubeFight::RecieveMessage(Message* message)
	{
		if (this->_HeroCubeObjectPtr)
		{
			this->_HeroCubeObjectPtr->RecieveMessage(message);
		}

		// Check for mouse move
		MouseMove* mouseMovePtr = dynamic_cast<MouseMove*>(message);

		// Check for key press messages
		MessageKeyDown* messageKeyDownPtr;
		messageKeyDownPtr = dynamic_cast<MessageKeyDown*>(message);
		if (messageKeyDownPtr)
		{
			// Check for spacebar message
			if (messageKeyDownPtr->Character == 32)
			{
				// Create a bullet
				GameObject* bullet = this->ObjectManagerPtr->CreateObject(BulletCubeMeshID, false);

				// Set its velocity to the hero direction scaled up
				D3DXVECTOR3 velocity = *this->_HeroCubeObjectPtr->GetVelocity();
				VectorMath::Normalize(&velocity, &velocity);
				VectorMath::ScaleVector(&velocity, &velocity, BULLET_SPEED);

				bullet->SetVelocity(velocity);
				bullet->Position = this->_HeroCubeObjectPtr->Position;
				VectorMath::Normalize(&velocity, &velocity);
				VectorMath::ScaleVector(&velocity, &velocity, this->_HeroCubeObjectPtr->Scale * 2.0f);
				bullet->Position += velocity;
				bullet->Scale = BULLET_SCALE;
			}
			// 'M'
			else if (messageKeyDownPtr->Character == 77)
			{
				// Create a missile
				GameObject* missile = this->ObjectManagerPtr->CreateObject(MissileCubeMeshID, false);

				// Set its velocity to the hero direction scaled up
				D3DXVECTOR3 velocity = *this->_HeroCubeObjectPtr->GetVelocity();
				VectorMath::Normalize(&velocity, &velocity);
				VectorMath::ScaleVector(&velocity, &velocity, MISSILE_SPEED);

				missile->SetVelocity(velocity);
				missile->Position = this->_HeroCubeObjectPtr->Position;
				VectorMath::Normalize(&velocity, &velocity);
				VectorMath::ScaleVector(&velocity, &velocity, this->_HeroCubeObjectPtr->Scale * 2.0f);
				missile->Position += velocity;
				missile->Scale = BULLET_SCALE;
			}
		}
		else if (mouseMovePtr)
		{
			float deltaX = LastMousePos.x - mouseMovePtr->MousePosition.x;
			float deltaY = LastMousePos.y - mouseMovePtr->MousePosition.y;
			
			//this->SceneCameraPtr->XZPlaneAngle += MOUSE_SENSITIVITY * deltaY;

			LastMousePos = mouseMovePtr->MousePosition;
		}
	}

	void SceneCubeFight::_RegisterCubeMesh(D3DXVECTOR4 cubeColor, string textureFilePath, unsigned int* outMeshID)
	{
		// Load all objects to be used in the scene onto the graphics card
		std::vector<SimpleVertex> cubeVertices;

		cubeVertices.push_back(SimpleVertex(D3DXVECTOR4(-0.5f, 0.5f, -0.5f, 1.0f), D3DXVECTOR2(0.0f, 0.0f)));
		cubeVertices.push_back(SimpleVertex(D3DXVECTOR4(0.5f, 0.5f, -0.5f, 1.0f), D3DXVECTOR2(1.0f, 0.0f)));
		cubeVertices.push_back(SimpleVertex(D3DXVECTOR4(0.5f, 0.5f, 0.5f, 1.0f), D3DXVECTOR2(1.0f, 1.0f)));
		cubeVertices.push_back(SimpleVertex(D3DXVECTOR4(-0.5f, 0.5f, 0.5f, 1.0f), D3DXVECTOR2(0.0f, 1.0f)));

		cubeVertices.push_back(SimpleVertex(D3DXVECTOR4(-0.5f, -0.5f, -0.5f, 1.0f), D3DXVECTOR2(0.0f, 0.0f)));
		cubeVertices.push_back(SimpleVertex(D3DXVECTOR4(0.5f, -0.5f, -0.5f, 1.0f), D3DXVECTOR2(1.0f, 0.0f)));
		cubeVertices.push_back(SimpleVertex(D3DXVECTOR4(0.5f, -0.5f, 0.5f, 1.0f), D3DXVECTOR2(1.0f, 1.0f)));
		cubeVertices.push_back(SimpleVertex(D3DXVECTOR4(-0.5f, -0.5f, 0.5f, 1.0f), D3DXVECTOR2(0.0f, 1.0f)));		
		
		std::vector<unsigned int> cubeIndices;

		cubeIndices.push_back(3), cubeIndices.push_back(1), cubeIndices.push_back(0);
		cubeIndices.push_back(2), cubeIndices.push_back(1), cubeIndices.push_back(3);

		cubeIndices.push_back(0), cubeIndices.push_back(5), cubeIndices.push_back(4);
		cubeIndices.push_back(1), cubeIndices.push_back(5), cubeIndices.push_back(0);

		cubeIndices.push_back(3), cubeIndices.push_back(4), cubeIndices.push_back(7);
		cubeIndices.push_back(0), cubeIndices.push_back(4), cubeIndices.push_back(3);

		cubeIndices.push_back(1), cubeIndices.push_back(6), cubeIndices.push_back(5);
		cubeIndices.push_back(2), cubeIndices.push_back(6), cubeIndices.push_back(1);

		cubeIndices.push_back(2), cubeIndices.push_back(7), cubeIndices.push_back(6);
		cubeIndices.push_back(3), cubeIndices.push_back(7), cubeIndices.push_back(2);

		cubeIndices.push_back(6), cubeIndices.push_back(4), cubeIndices.push_back(5);
		cubeIndices.push_back(7), cubeIndices.push_back(4), cubeIndices.push_back(6);

		SYSTEM_MANAGER->BroadcastMessage(
			&MessageRegisterObjectMesh(
				&cubeVertices,
				&cubeIndices,
				D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST,
				textureFilePath,
				outMeshID)
		);
	}

	void SceneCubeFight::_CollisionCheck(GameObject* nextObject1)
	{
		// If the first object is the floor, skip
		if (nextObject1->Type == FloorMeshID)
		{
			return;
		}

		// Check for collision with each other
		for (unsigned int j = 0; j < this->ObjectManagerPtr->HighestLiveIndex; ++j)
		{
			GameObject* nextObject2 = this->ObjectManagerPtr->ObjectArray[j];

			// Skip checks against the floor
			if(nextObject2->Type == FloorMeshID || nextObject2->LifeState != ALIVE)
			{
				continue;
			}

			// If collision
			if (PhysicsComponent::AreColliding(nextObject1, nextObject2))
			{
				PhysicsComponent::BounceObjectsOffEachOtherIfColliding(nextObject1, nextObject2);

				// If either one is a bullet or missile destroy it
				if (nextObject1->Type == MissileCubeMeshID || nextObject1->Type == BulletCubeMeshID)
				{
					nextObject1->Destroy();
				}
				
				if(nextObject2->Type == MissileCubeMeshID || nextObject2->Type == BulletCubeMeshID)
				{
					nextObject2->Destroy();
				}
			}
		}
	}

	void SceneCubeFight::_RefreshText()
	{
		// Delete if already created
		if (this->_ScoreText)
		{
			delete this->_ScoreText;
			this->_ScoreText = 0;
		}

		if (this->_LivesText)
		{
			delete this->_LivesText;
			this->_LivesText = 0;
		}

		int firstTop = 50;
		int deltaHeight = 40;
		int width = 200;
		int height = 30;
		int left = 5;
		RECT location;
		location.bottom = firstTop + height;
		location.top = firstTop;
		location.left = left;
		location.right = left + width;

		string text = "Score: " + convertInt(this->_Score);

		this->_ScoreText = new MessageDrawText(text, location, TEXT_COLOR);

		location.bottom += deltaHeight;
		location.top += deltaHeight;

		text = "Lives: " + convertInt(this->_LifeCount);

		this->_LivesText = new MessageDrawText(text, location, TEXT_COLOR);
	}
}