/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: LineSegment.cpp
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "Precompiled.h"
#include "LineSegment.h"

namespace Framework
{
	LineSegment::LineSegment()
	{
		this->Pos1 = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		this->Pos2 = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		this->XZNormal = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	}

	void LineSegment::Initialize(D3DXVECTOR3* pos1, D3DXVECTOR3* pos2)
	{
		this->Pos1 = *pos1;
		this->Pos2 = *pos2;

		D3DXVECTOR3 vec1To2 = Pos2 - Pos1;
		VectorMath::GetXZNormal(&vec1To2, &this->XZNormal);
		VectorMath::Normalize(&this->XZNormal, &this->XZNormal);
	}

	bool LineSegment::IsPointOnNormalSide(const D3DXVECTOR3* pos)
	{
		// Scale normal down to small amount
		D3DXVECTOR3 smallNormal;
		VectorMath::Normalize(&this->XZNormal, &smallNormal);
		VectorMath::ScaleVector(&smallNormal, &smallNormal, 0.0000001f);

		// Move the input postion by the small normal
		D3DXVECTOR3 movedPos = *pos + smallNormal;

		float distanceBefore = VectorMath::DistanceBetweenPoints(pos, &this->Pos1);
		float distanceAfter = VectorMath::DistanceBetweenPoints(&movedPos, &this->Pos1);

		if (distanceBefore > distanceAfter)
		{
			// Moving by a small normal got it closer, it is on the neg normal side
			return false;
		}
		else if (distanceBefore < distanceAfter)
		{
			// Moving by a small amount got it farther away from the line, it is on the pos normal side
			return true;
		}
		
		ErrorIf(true, "Point on normal side function failed to tell which side");

		// Make the compiler happy
		return true;
	}
}

