/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: LoggingManager.cpp
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "LoggingManager.h"

namespace Framework
{
	const bool All = true;
	const bool None = false;
	const bool Quit = false;
	const bool Collide = false;
	const bool GameOver = false;
	const bool ToggleDebugInfo = false;
	const bool CharacterKey = false;
	const bool KeyDown = false;
	const bool KeyUp = false;
	const bool MouseButton = false;
	const bool MouseMove = false;
	const bool FileDrop = false;
	const bool RegisterObjectMesh = false;
	const bool RegisterObjectTexture = false;
	const bool DrawObject = false;
	const bool DrawText = false;
	const bool LogMessage = false;
	const bool Save = false;
	const bool Load = false;
	const bool Continue = false;
	const bool NewGame = false;

	void LoggingManager::Initialize()
	{
		// Delete old log file if it exists
		this->logFile.open("log.txt");
	}

	void LoggingManager::RecieveMessage(Message* message)
	{
		// Write description of message to log file
		if (All)
		{
			this->logFile << message->GetDescription() << '\n';
		}
	}
}