/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: DrawingComponent.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "GameObjectComponent.h"
#include "Precompiled.h"
#include "SystemManager.h"
#include "Camera.h"
#include "WindowsSystem.h"
#include "Message.h"

namespace Framework
{
	class DrawingComponent : public GameObjectComponent
	{
	public:
		static const bool DEBUG_DRAWING = true;

		DrawingComponent(unsigned int objectMeshID);
		~DrawingComponent();

		virtual void Initialize();
		virtual void RecieveMessage(Message* message);

		void DrawObject(Camera* cameraPtr);

	private:
		static void _RegisterDebugArrowMesh(D3DXVECTOR4 arrowColor, unsigned int* outMeshID);
		void _DrawDebugArrows(Camera* cameraPtr);

		// Graphics mesh id
		unsigned int _MeshID;

		// GraphicsManager pipeline matrices
		D3DXMATRIX _WorldMatrix;

		D3DXMATRIX _WorldMatrixDebug;

		GOC* _DebugArrowVelocity;
		GOC* _DebugArrowAccelaration;
	};
}