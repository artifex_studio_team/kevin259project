/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: GameObjectManager.cpp
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "GameObjectManager.h"
#include "GameObject.h"
#include "WindowsSystem.h"
using namespace std;

namespace Framework
{
	const unsigned int MAX_OBJECTS = 1000;
	GameObjectManager::GameObjectManager()
	{
		// Create array of objects
		this->ObjectArray = new GameObject* [MAX_OBJECTS];
		for(int i = 0; i < MAX_OBJECTS; ++i)
		{
			this->ObjectArray[i] = new GameObject();
		}

		this->HighestLiveIndex = 0;
	}

	GameObjectManager::GameObjectManager(ifstream* reader)
	{
		// Create array of objects
		this->ObjectArray = new GameObject* [MAX_OBJECTS];
		this->HighestLiveIndex = 0;

		string nextLine;

		// Make sure header is ok
		getline(*reader, nextLine);
		ErrorIf(nextLine.compare("<ObjectsSection>"), "Invalid file used to load");

		// Read object count
		getline(*reader, nextLine);
		this->HighestLiveIndex = atoi(nextLine.c_str()) - 1;

		ErrorIf (this->HighestLiveIndex >= MAX_OBJECTS, "More than max objects attempted to be loaded from disk");

		// Read the objects
		for (unsigned int i = 0 ; i <= this->HighestLiveIndex; ++i)
		{
			this->ObjectArray[i] = new GameObject(reader);
		}

		// Create the rest of the objects array
		for (int j = this->HighestLiveIndex + 1; j < MAX_OBJECTS; ++j)
		{
			this->ObjectArray[j] = new GameObject();
		}

		// Make sure footer is ok
		getline(*reader, nextLine);
		ErrorIf(nextLine.compare("</ObjectsSection>"), "Invalid file used to load");
	}

	GameObjectManager::~GameObjectManager()
	{
		for (int i = 0; i < MAX_OBJECTS; ++i)
		{
			delete this->ObjectArray[i];
		}

		delete[] this->ObjectArray;
	}

	GameObject* GameObjectManager::CreateObject(unsigned int meshID, bool isStatic)
	{
		for (unsigned int i = 0; i < MAX_OBJECTS; ++i)
		{
			// Find first dead
			if (this->ObjectArray[i]->LifeState == DEAD)
			{
				// Initialize it with mesh id
				this->ObjectArray[i]->Initialize(meshID, isStatic);

				// REcord highest live index if needed
				if (i > this->HighestLiveIndex)
				{
					this->HighestLiveIndex = i;
				}

				return this->ObjectArray[i];
			}
		}

		// We have exceeded max objects
		return 0;
	}

	void GameObjectManager::DrawAllLiveObjects(Camera* cameraPtr)
	{
		for (unsigned int i = 0; i <= this->HighestLiveIndex; ++i)
		{
			// If the object is alive
			if (this->ObjectArray[i]->LifeState == ALIVE)
			{
				// Draw it
				this->ObjectArray[i]->Draw(cameraPtr);
			}
		}
	}

	void GameObjectManager::DestroyAllObjects()
	{
		for (unsigned int i = 0; i <= this->HighestLiveIndex; ++i)
		{
			// If the object is alive
			if (this->ObjectArray[i]->LifeState == ALIVE)
			{
				// Destroy it
				this->ObjectArray[i]->Destroy();
			}
		}
	}

	std::string GameObjectManager::ToString()
	{
		std::stringstream builder;

		// Write header line
		builder << "<ObjectsSection>" << '\n';

		// Count the objects
		int count = 0;
		for (unsigned int i = 0; i <= this->HighestLiveIndex; ++i)
		{
			// If the object is alive
			if (this->ObjectArray[i]->LifeState == ALIVE)
			{
				++count;
			}
		}

		// Record the count
		builder << count << '\n';

		for (unsigned int i = 0; i <= this->HighestLiveIndex; ++i)
		{
			// If the object is alive
			if (this->ObjectArray[i]->LifeState == ALIVE)
			{
				builder << this->ObjectArray[i]->ToString();
			}
		}

		// Write footer
		builder << "</ObjectsSection>" << '\n';

		return builder.str();
	}
}