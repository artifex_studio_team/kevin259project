/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: PhysicsComponent.cpp
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "PhysicsComponent.h"
#include "GameObject.h"

namespace Framework
{
	static const float PI = 3.1415926535897f;
	static const float HOMING_SPEED_LIMIT = 0.99f;
	static const float DEFAULT_SPEED_LIMIT = 0.9f;

	void PhysicsComponent::HomeObjectOnTarget(GameObject* obj1, GameObject* target)
	{
		// Return if same object
		if (obj1->GetId() == target->GetId())
		{
			return;
		}

		D3DXVECTOR3 vecToTarget = target->Position - obj1->Position;
		
		// Normalize and scale
		VectorMath::Normalize(&vecToTarget, &vecToTarget);
		VectorMath::ScaleVector(&vecToTarget, &vecToTarget, 0.5f);

		vecToTarget += *obj1->GetVelocity();
		VectorMath::ScaleVector(&vecToTarget, &vecToTarget, HOMING_SPEED_LIMIT);
		obj1->SetVelocity(vecToTarget);
	}

	bool PhysicsComponent::AreColliding(GameObject* obj1, GameObject* obj2)
	{
		// Return if same object
		if (obj1->GetId() == obj2->GetId())
		{
			return false;
		}

		// Get the vector from center point to center point
		D3DXVECTOR3 vec1To2 = obj2->Position - obj1->Position;
		D3DXVECTOR3 vec2To1 = obj1->Position - obj2->Position;

		// See how long it is
		float distBetweenCenters = VectorMath::VectorLength(&vec1To2);
		float scaleSum = obj1->Scale + obj2->Scale;

		// If it is within the sum of the scales, collision
		if (distBetweenCenters < scaleSum)
		{
			return true;
		}

		return false;
	}

	void PhysicsComponent::BounceObjectsOffEachOtherIfColliding(GameObject* obj1, GameObject* obj2)
	{
		// Get the vector from center point to center point
		D3DXVECTOR3 vec1To2 = obj2->Position - obj1->Position;
		D3DXVECTOR3 vec2To1 = obj1->Position - obj2->Position;

		// See how long it is
		float distBetweenCenters = VectorMath::VectorLength(&vec1To2);
		float scaleSum = obj1->Scale + obj2->Scale;

		float moveDistance = scaleSum - distBetweenCenters;

		// If move distance is negative no collision
		if (moveDistance <= 0 ) 
		{
			return;
		}

		// Move object 2 to point of intersection
		VectorMath::Normalize(&vec1To2, &vec1To2);
		VectorMath::ScaleVector(&vec1To2, &vec1To2, moveDistance / 2.0f);
		obj2->Position = obj2->Position + vec1To2;

		// Move object 1 to point of intersection
		obj1->Position = obj1->Position - vec1To2;

		// If angle between center to center vector and velocity vector < 90
		float angle1 = VectorMath::SmallestAngleBetweenXZPlane(obj1->GetVelocity(), &vec1To2);
		float pushLength1 = 0.0f;
		D3DXVECTOR3 pushFrom1To2;

		if (angle1 < (3.1415926535897f / 2.0f))
		{
			// Find the component of the velocity vector pushing towards other center
			float velocityLength = VectorMath::VectorLength(obj1->GetVelocity());
			pushLength1 = velocityLength * cos(angle1);
			VectorMath::Normalize(&vec1To2, &pushFrom1To2);
			VectorMath::ScaleVector(&pushFrom1To2, &pushFrom1To2, pushLength1);
		}

		float angle2 = VectorMath::SmallestAngleBetweenXZPlane(obj2->GetVelocity(), &vec2To1);
		float pushLength2 = 0.0f;
		D3DXVECTOR3 pushFrom2To1;

		if (angle2 < (3.1415926535897f / 2.0f))
		{
			// Find the component of the velocity vector pushing towards other center
			float velocityLength = VectorMath::VectorLength(obj2->GetVelocity());
			pushLength2 = velocityLength * cos(angle2);
			VectorMath::Normalize(&vec2To1, &pushFrom2To1);
			VectorMath::ScaleVector(&pushFrom2To1, &pushFrom2To1, pushLength2);
		}

		// Eliminate any y reflection
		pushFrom1To2.y = 0.0f;
		pushFrom2To1.y = 0.0f;

		// Add/ subtract exchange of velocity between objects
		if (!obj1->IsStatic && !obj2->IsStatic)
		{
			// Both dynamic objects
			if (pushLength1 > 0.0f)
			{
				obj1->SetVelocity(*obj1->GetVelocity() - pushFrom1To2);
				obj2->SetVelocity(*obj2->GetVelocity() + pushFrom1To2);
			}

			if (pushLength2 > 0.0f)
			{
				obj1->SetVelocity(*obj1->GetVelocity() + pushFrom2To1);
				obj2->SetVelocity(*obj2->GetVelocity() - pushFrom2To1);
			}
		}
		else if (obj1->IsStatic && !obj2->IsStatic)
		{
			// Obj 1 is static, obj 2 dynamic
			// Double push back to get reflection
			VectorMath::ScaleVector(&pushFrom2To1, &pushFrom2To1, -2.0f);
			obj2->SetVelocity(*obj2->GetVelocity() + pushFrom2To1);
		}
		else if (!obj1->IsStatic && obj2->IsStatic)
		{
			// Obj 1 is dynamic, obj 2 is static
			// Double push back to get reflection
			VectorMath::ScaleVector(&pushFrom1To2, &pushFrom1To2, -2.0f);
			obj1->SetVelocity(*obj1->GetVelocity() + pushFrom1To2);
		}
		else
		{
			ErrorIf(true, "Collision between two static objects shouldn't happen");
		}
	}

	void PhysicsComponent::Initialize()
	{
		this->MovementAccelaration = 15.0f;
		this->BrakeStrengthPerFrame = 0.05f;
		this->TurningVelocity = 600.0f;
		this->SpeedLimit = DEFAULT_SPEED_LIMIT;
		this->AccelarateBackwardsOn = false;
		this->AccelarateForewardsOn = false;
		this->TurnLeftOn = false;
		this->TurnRightOn = false;
	}

	void PhysicsComponent::_Accelarate(bool foreward, float timeSlice)
	{
		// Create vector on xz plane of length 1 on x axis
		D3DXVECTOR3 accelaration = this->GetOwner()->GetHeadingXZPlane();

		// Scale
		accelaration.x = accelaration.x * this->MovementAccelaration;
		accelaration.z = accelaration.z * this->MovementAccelaration;

		VectorMath::ScaleVector(&accelaration, &accelaration, timeSlice);

		// Update velocity based on accelaration
		if (foreward)
		{
			this->GetOwner()->SetVelocity(*this->GetOwner()->GetVelocity() + accelaration);

			// Scale in speed limimt
			D3DXVECTOR3 tempVelocity;
			VectorMath::ScaleVector(this->GetOwner()->GetVelocity(), &tempVelocity, this->SpeedLimit);
			this->GetOwner()->SetVelocity(tempVelocity);
		}
		else
		{
			D3DXVECTOR3 tempVelocity = *this->GetOwner()->GetVelocity() - accelaration;

			// Scale in speed limit
			VectorMath::ScaleVector(this->GetOwner()->GetVelocity(), &tempVelocity, this->SpeedLimit);
			this->GetOwner()->SetVelocity(tempVelocity);
		}
	}

	void PhysicsComponent::UpdatePosition(float timeSlice)
	{
		if (this->AccelarateForewardsOn && !this->AccelarateBackwardsOn)
		{
			// Forewards
			this->_Accelarate(true, timeSlice);
		}
		else if (!this->AccelarateForewardsOn && this->AccelarateBackwardsOn)
		{
			// Backwards
			D3DXVECTOR3 tempVelocity;
			VectorMath::ScaleVector(this->GetOwner()->GetVelocity(), &tempVelocity, 1.0f - this->BrakeStrengthPerFrame);
			this->GetOwner()->SetVelocity(tempVelocity);
		}

		if (this->TurnRightOn && !this->TurnLeftOn)
		{
			// Turn right
			this->_Turn(true, timeSlice);
		}
		else if (!this->TurnRightOn && this->TurnLeftOn)
		{
			// Turn left
			this->_Turn(false, timeSlice);
		}

		// Scale velocity by time slice
		D3DXVECTOR3 velocity = *this->GetOwner()->GetVelocity();
		VectorMath::ScaleVector(&velocity, &velocity, timeSlice);

		// Update position based on velocity
		this->GetOwner()->Position += velocity;
	}

	void PhysicsComponent::_Turn(bool right, float timeSlice)
	{
		// Rotate object
		float rotationYVel = this->TurningVelocity * timeSlice;

		if (right)
		{
			this->GetOwner()->SetRotationY(*this->GetOwner()->GetRotationY() - (rotationYVel * timeSlice));
		}
		else
		{
			this->GetOwner()->SetRotationY(*this->GetOwner()->GetRotationY() + (rotationYVel * timeSlice));
		}
	}

	D3DXVECTOR3 PhysicsComponent::RotationYToVelocity(float rotationY, float scale)
	{
		// Rotate velocity
		D3DXVECTOR3 newVel;
		newVel.x = cos(rotationY);
		newVel.y = 0.0f;
		newVel.z = sin(rotationY);

		// Scale properly and copy into owner velocity vector
		VectorMath::ScaleVector(&newVel, &newVel, scale);

		return newVel;
	}

	float PhysicsComponent::VelocityToRotationY(D3DXVECTOR3 velocity)
	{
		D3DXVECTOR3 normalizedVel;
		VectorMath::Normalize(&velocity, &normalizedVel);
		float angleFromX = acos(normalizedVel.x);
		float angleFromZ = asin(normalizedVel.z);

		if (angleFromZ >= 0)
		{
			// Quadrant 1 or 2, use angle from x
			return angleFromX;
		}
		else if (angleFromZ < 0)
		{
			// Quadrant 3 or 4, sweep clockwise instead
			return 2.0f * PI - angleFromX;
		}

		ErrorIf(true, "Unreachable code reached");
		return 0.0f; // Makes compiler happy
	}

	void PhysicsComponent::RecieveMessage(Message* message)
	{
		// Check for key press messages
		MessageKeyDown* messageKeyDownPtr;
		messageKeyDownPtr = dynamic_cast<MessageKeyDown*>(message);
		if (messageKeyDownPtr)
		{
			// 's'
			if (messageKeyDownPtr->Character == 83)
			{
				this->AccelarateBackwardsOn = true;
			}
			// 'a'
			else if (messageKeyDownPtr->Character == 65)
			{
				this->TurnLeftOn = true;
			}
			// 'd'
			else if (messageKeyDownPtr->Character == 68)
			{
				this->TurnRightOn = true;
			}
			// 'w'
			else if (messageKeyDownPtr->Character == 87)
			{
				this->AccelarateForewardsOn = true;
			}
		}

		MessageKeyUp* messageKeyUpPtr;
		messageKeyUpPtr = dynamic_cast<MessageKeyUp*>(message);
		if (messageKeyUpPtr)
		{
			// 's'
			if (messageKeyUpPtr->Character == 83)
			{
				this->AccelarateBackwardsOn = false;
			}
			// 'a'
			else if (messageKeyUpPtr->Character == 65)
			{
				this->TurnLeftOn = false;
			}
			// 'd'
			else if (messageKeyUpPtr->Character == 68)
			{
				this->TurnRightOn = false;
			}
			// 'w'
			else if (messageKeyUpPtr->Character == 87)
			{
				this->AccelarateForewardsOn = false;
			}
		}
	}
}