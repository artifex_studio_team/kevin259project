/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: GraphicsManager.cpp
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "GraphicsManager.h"
#include "WindowsSystem.h"

using namespace std;
namespace Framework
{
	static unsigned int MeshIDCounter = 0;
	static unsigned int TextureIDCounter = 0;

	//Set everything to default values.
	GraphicsManager::GraphicsManager(HWND hWnd, int screenWidth, int screenHeight)
	{
		this->HWnd = hWnd;
		this->ScreenWidth = screenWidth;
		this->ScreenHeight = screenHeight;
		this->frameRate = 1.0f / 60.0f;
	}

	//Initializes Direct3D
	void GraphicsManager::Initialize()
	{
		DXGI_SWAP_CHAIN_DESC swapChainDescription;
		ZeroMemory(&swapChainDescription, sizeof(swapChainDescription));
		swapChainDescription.BufferCount = 1;
		swapChainDescription.BufferDesc.Width = this->ScreenWidth;
		swapChainDescription.BufferDesc.Height = this->ScreenHeight;
		swapChainDescription.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		swapChainDescription.BufferDesc.RefreshRate.Numerator = 60;
		swapChainDescription.BufferDesc.RefreshRate.Denominator = 1;
		swapChainDescription.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		swapChainDescription.OutputWindow = this->HWnd;
		swapChainDescription.SampleDesc.Count = 1;
		swapChainDescription.SampleDesc.Quality = 0;
		swapChainDescription.Windowed = TRUE;

		// Create the swap chain, device, and device context objects
		HRESULT createChainDeviceContextResult = D3D10CreateDeviceAndSwapChain(
			NULL,
			D3D10_DRIVER_TYPE_HARDWARE,
			NULL,
			D3D10_CREATE_DEVICE_DEBUG,
            D3D10_SDK_VERSION,
			&swapChainDescription,
			&this->SwapChainPtr,
			&this->DevicePtr);

		// If the swap chain, device, device context creation failed
		ErrorIf(FAILED(createChainDeviceContextResult));

		// Create a render target view
		ID3D10Texture2D* backBufferPtr;
		
		HRESULT getBackBufferResult = this->SwapChainPtr->GetBuffer(
			0,
			__uuidof(ID3D10Texture2D),
			(LPVOID*)&backBufferPtr);

		ErrorIf(FAILED(getBackBufferResult));

		// Create render target view
		HRESULT createRenderTargetResult = this->DevicePtr->CreateRenderTargetView(
			backBufferPtr,
			NULL,
			&this->RenderTargetViewPtr);
		backBufferPtr->Release();

		ErrorIf(FAILED(createRenderTargetResult));

		this->_CreateDepthStencilBuffer();

		// Set the render targets in the device context
		this->DevicePtr->OMSetRenderTargets(1, &this->RenderTargetViewPtr, this->DepthStencilViewPtr);

		// Initialize the viewport
		this->ViewPort.Width = this->ScreenWidth;
		this->ViewPort.Height = this->ScreenHeight;
		this->ViewPort.MinDepth = 0.0f;
		this->ViewPort.MaxDepth = 1.0f;
		this->ViewPort.TopLeftX = 0;
		this->ViewPort.TopLeftY = 0;

		// Create a default effect
		this->_CreateEffectGroup(&this->CurrentEffectGroupPtr, "SimpleShader.fx");

		// Create the layout description
		D3D10_INPUT_ELEMENT_DESC layout[] = 
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D10_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 16, D3D10_INPUT_PER_VERTEX_DATA, 0 }
		};
		unsigned int numElements = sizeof(layout)/sizeof(layout[0]);

		// Create the input layout
		D3D10_PASS_DESC passDescription;
		this->CurrentEffectGroupPtr->EffectTechniquePtr->GetPassByIndex(0)->GetDesc(&passDescription);

		ID3D10InputLayout* inputLayoutPtr;
		HRESULT createInputLayoutResult = this->DevicePtr->CreateInputLayout(
			layout,
			numElements,
			passDescription.pIAInputSignature,
			passDescription.IAInputSignatureSize,
			&inputLayoutPtr);

		ErrorIf(FAILED(createInputLayoutResult), "Input layout creation failed");

		this->DevicePtr->IASetInputLayout(inputLayoutPtr);

		// Projection matrix with 90 or(pi/2) virtical field of view,
		// aspect ratio of width/ height, .1 front clipping plane, 100 back clipping plane
		this->_ProjectionMatrix;
		D3DXMatrixPerspectiveFovLH(
			&this->_ProjectionMatrix,
			3.1415926f/2.0f,
			(float)this->ScreenWidth/(float)this->ScreenHeight,
			.0001f,
			100.0f);

		// Initialize camera
		this->_CameraPtr = new Camera(D3DXVECTOR3(0.0f, 1.0f, 5.0f), D3DXVECTOR3(0.0f, 1.0f, 0.0f));

		// Initialize debug writing font
		HRESULT fontCreationResult = D3DX10CreateFont(
			this->DevicePtr,
			20,
			20,
			FW_BOLD,
			1,
			FALSE,
			DEFAULT_CHARSET,
			OUT_DEFAULT_PRECIS,
			DEFAULT_QUALITY,
			DEFAULT_PITCH | FF_DONTCARE,
			TEXT("Arial"),
			&this->DebugWritingFont);

		ErrorIf(FAILED(fontCreationResult), "Font creation failed");

		HRESULT spriteCreationResult = D3DX10CreateSprite(
			this->DevicePtr,
			0,
			&this->DebugTextSprite);

		ErrorIf(FAILED(spriteCreationResult), "Sprite creation failed");

	}

	void GraphicsManager::_SetPipelineMatrices(D3DXMATRIX* worldMatrix, D3DXMATRIX* viewMatrix)
	{
		this->CurrentEffectGroupPtr->
			SetTranformationMatricies(worldMatrix, viewMatrix, &this->_ProjectionMatrix);
	}

	//Release Direct3D and do any needed cleanup.
	GraphicsManager::~GraphicsManager()
	{
		if (this->_CameraPtr)
		{
				delete this->_CameraPtr;
		}
	}

	void GraphicsManager::_DrawText(std::string text, RECT locationRect, D3DXVECTOR4 fontColor)
	{
		// Manually save and restore depth buffer stencil state
		ID3D10DepthStencilState* dss;
		unsigned int foo = 1;
		this->DevicePtr->OMGetDepthStencilState(&dss, &foo);

		HRESULT result = this->DebugTextSprite->Begin(D3DX10_SPRITE_SAVE_STATE);

		ErrorIf(FAILED(result), "Sprite Text Begin failed");

		// Draw some text 
		D3DXCOLOR color(fontColor.x, fontColor.y, fontColor.z, fontColor.w);
		this->DebugWritingFont->DrawText(this->DebugTextSprite, text.c_str(), -1, &locationRect, 0, color);

		this->DebugTextSprite->End();

		// Restore depth stencil state
		this->DevicePtr->OMSetDepthStencilState(dss, 1);
	}

	// Draw stoof
	void GraphicsManager::Update(float dt)
	{
		// If the draw time was less than 60 hz ago, wait until it is
		float timeDiff = this->frameRate - dt;
		if (timeDiff > 0)
		{
			// You are feeling very sleepy
			Sleep((DWORD)(timeDiff * 1000.0f));
		}

		// Clear the render target, draw all blue background
		float clearColor[4] = { 0.0f, 0.125f, 0.6f, 0.5f };
		this->DevicePtr->ClearRenderTargetView(this->RenderTargetViewPtr, clearColor);

		// Draw all the pending objects
		while (this->_PendingDrawObjectMessages.size() > 0)
		{
			MessageDrawObject drawMessage = this->_PendingDrawObjectMessages.back();
			this->_PendingDrawObjectMessages.pop_back();

			this->_DrawObject(drawMessage.WorldMatrixPtr, drawMessage.ViewMatrixPtr, drawMessage.ObjectMeshID);
		}

		// Draw all the pending text
		while (this->_PendingDrawTextMessages.size() > 0)
		{
			MessageDrawText drawMessage = this->_PendingDrawTextMessages.back();
			this->_PendingDrawTextMessages.pop_back();
			this->_DrawText(drawMessage.Text, drawMessage.LocationRect, drawMessage.FontColor);
		}

		// Draw frame time
		RECT rct;
		rct.left=2;
		rct.right=780;
		rct.top=10;
		rct.bottom=rct.top+20;
		stringstream sstr;
		sstr << "Frame time (sec): ";
		sstr << dt;
		this->_DrawText(sstr.str().c_str(), rct, D3DXVECTOR4(256, 0, 0, 256));

		this->DevicePtr->ClearDepthStencilView(this->DepthStencilViewPtr, D3D10_CLEAR_DEPTH|D3D10_CLEAR_STENCIL, 1.0f, 0);
		this->DevicePtr->RSSetViewports(1, &this->ViewPort);
		this->DevicePtr->IASetIndexBuffer(this->IndexBufferPtr, DXGI_FORMAT_R32_UINT, 0);

		// Swap the buffers
		this->SwapChainPtr->Present(1, 0);
	}

	void GraphicsManager::RecieveMessage(Message* message)
	{
		// Register mesh
		MessageRegisterObjectMesh* registerMeshMessagePtr;
		registerMeshMessagePtr = dynamic_cast<MessageRegisterObjectMesh*>(message);
		if (registerMeshMessagePtr)
		{
			this->_RegisterObjectMesh(
				registerMeshMessagePtr->VerticesVectorPtr,
				registerMeshMessagePtr->IndicesVectorPtr,
				registerMeshMessagePtr->TextureFilePath,
				registerMeshMessagePtr->Topology,
				registerMeshMessagePtr->OutObjectMeshIDPtr);
		}

		// Draw object
		MessageDrawObject* drawObjectMessagePtr;
		drawObjectMessagePtr = dynamic_cast<MessageDrawObject*>(message);
		if (drawObjectMessagePtr)
		{
			this->_PendingDrawObjectMessages.push_back(*drawObjectMessagePtr);
		}

		// Draw text
		MessageDrawText* drawTextMessagePtr;
		drawTextMessagePtr = dynamic_cast<MessageDrawText*>(message);
		if (drawTextMessagePtr)
		{
			this->_PendingDrawTextMessages.push_back(*drawTextMessagePtr);
		}
	}

	void GraphicsManager::_CreateEffectGroup(EffectGroup** effectGroupReference, char* fileName)
	{
		ID3D10Effect* effectPtr;
		ID3D10Blob* errorsBlob;
		HRESULT effectCreationResult = 
			D3DX10CreateEffectFromFile(
				"SimpleShader.fx",
				NULL,
				NULL,
				"fx_4_0",
				D3D10_SHADER_DEBUG,
				0,
				this->DevicePtr,
				NULL,
				NULL,
				&effectPtr,
				&errorsBlob,
				NULL);

		std::string errorMessages = (std::string)DXGetErrorString(effectCreationResult);
		if (errorsBlob)
		{
			errorMessages += (char*)errorsBlob->GetBufferPointer();
		}

		ErrorIf(FAILED(effectCreationResult), errorMessages.c_str());

		(*effectGroupReference) = new EffectGroup(effectPtr);
	}

	void GraphicsManager::_RegisterObjectMesh(
		std::vector<SimpleVertex>* objectVertices,
		std::vector<unsigned int>* objectDrawIndices,
		string textureFilePath,
		D3D10_PRIMITIVE_TOPOLOGY topology,
		unsigned int* outMeshIDPtr)
	{
		// Assign an ID to the new mesh
		(*outMeshIDPtr) = MeshIDCounter++;

		// Create new mesh
		ObjectMesh* newMeshPtr = new ObjectMesh();
		newMeshPtr->IndexVector = (*objectDrawIndices);
		newMeshPtr->VertexVector = (*objectVertices);
		newMeshPtr->IndexBufferOffset = this->_RunningIndexVector.size();
		newMeshPtr->VertexBufferOffset = this->_RunningVertexVector.size();
		newMeshPtr->Topology = topology;

		// Load and register texture
		this->_RegisterObjectTexture(textureFilePath, &newMeshPtr->TextureID);

		this->_RegisteredMeshVector.push_back(newMeshPtr);

		// Add to running vertex list
		for (unsigned int i = 0; i < objectVertices->size(); ++i)
		{
			this->_RunningVertexVector.push_back((*objectVertices)[i]);
		}

		// Add to running index list
		for (unsigned int i = 0; i < objectDrawIndices->size(); ++i)
		{
			this->_RunningIndexVector.push_back((*objectDrawIndices)[i]);
		}

		// Update the buffers on the graphics card
		this->_CreateAndBindVertexBuffer();
		this->_CreateAndBindIndexBuffer();
	}

	void GraphicsManager::_DrawObject(D3DXMATRIX* objectWorldMatrixPtr, D3DXMATRIX* viewMatrixPtr, unsigned int objectMeshID)
	{	
		// Set the transformation matrices specific to this object
		this->_SetPipelineMatrices(objectWorldMatrixPtr, viewMatrixPtr);

		// Get the technique description
		D3D10_TECHNIQUE_DESC techDesc;
		this->CurrentEffectGroupPtr->EffectTechniquePtr->GetDesc(&techDesc);

		// Get the mesh for the object
		if (this->_RegisteredMeshVector.size() <= objectMeshID || objectMeshID < 0)
		{
			ErrorIf(true, "Attempted to draw object with invalid mesh id");
		}
		ObjectMesh* objectMeshPtr = this->_RegisteredMeshVector[objectMeshID];

		// Check for valid texture id
		if (objectMeshPtr->TextureID >= this->_RegisteredTextureVector.size())
		{
			ErrorIf(true, "Attempted to draw object with invalid texture id");
		}

		// Texture binding code
		ID3D10EffectShaderResourceVariable* textureResourceVariable = 
			this->CurrentEffectGroupPtr->EffectPtr->GetVariableByName("txDiffuse")->AsShaderResource();
		ID3D10ShaderResourceView* textureResourceView = this->_RegisteredTextureVector[objectMeshPtr->TextureID];
		textureResourceVariable->SetResource(textureResourceView);		

		// Set vertex topology
		this->DevicePtr->IASetPrimitiveTopology(objectMeshPtr->Topology);

		// Draw each pass in the description
		for(unsigned int p = 0; p < techDesc.Passes; ++p)
		{
			// Apply each pass in the shader to the vertices
			this->CurrentEffectGroupPtr->EffectTechniquePtr->GetPassByIndex(p)->Apply(0);

			// Draw vertices
			this->DevicePtr->DrawIndexed(objectMeshPtr->IndexVector.size(), objectMeshPtr->IndexBufferOffset, objectMeshPtr->VertexBufferOffset);
		}
	}

	void GraphicsManager::_CreateAndBindVertexBuffer()
	{
		if (this->_RunningVertexVector.size() <= 0)
		{
			ErrorIf(true, "Vertex buffer creation attempted with empty vertex list");
		}

		// Description of the vertex buffer
		D3D10_BUFFER_DESC bd;
		bd.Usage = D3D10_USAGE_DEFAULT;
		bd.ByteWidth = sizeof( SimpleVertex ) * this->_RunningVertexVector.size();
		bd.BindFlags = D3D10_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		bd.MiscFlags = 0;

		// Description of the data to be copied into the vertex buffer
		D3D10_SUBRESOURCE_DATA InitData; 
		InitData.pSysMem = &this->_RunningVertexVector[0];

		ID3D10Buffer* vertexBufferPtr;
		HRESULT createVertexBufferResult = 
			this->DevicePtr->CreateBuffer(&bd, &InitData, &vertexBufferPtr);

		ErrorIf(FAILED(createVertexBufferResult), "Vertex buffer creation failed");

		// Bind the vertex buffer to the device
		UINT stride = sizeof(SimpleVertex);
		UINT offset = 0;
		this->DevicePtr->IASetVertexBuffers(0, 1, &vertexBufferPtr, &stride, &offset);
	}

	void GraphicsManager::_CreateDepthStencilBuffer()
	{
		// Create depth stencil texture
		D3D10_TEXTURE2D_DESC descDepth;
		descDepth.Width = this->ScreenWidth;
		descDepth.Height = this->ScreenHeight;
		descDepth.MipLevels = 1;
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT_D32_FLOAT;
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D10_USAGE_DEFAULT;
		descDepth.BindFlags = D3D10_BIND_DEPTH_STENCIL;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;

		ID3D10Texture2D* depthStencilPtr;
		HRESULT textureCreationResult = this->DevicePtr->CreateTexture2D(&descDepth, NULL, &depthStencilPtr);

		ErrorIf(FAILED(textureCreationResult), "Depth stencil texture creation failed");

		// Create the depth stencil view
		D3D10_DEPTH_STENCIL_VIEW_DESC descDSV;
		descDSV.Format = descDepth.Format;
		descDSV.ViewDimension = D3D10_DSV_DIMENSION_TEXTURE2D;
		descDSV.Texture2D.MipSlice = 0;
		HRESULT depthStencilResult = this->DevicePtr->CreateDepthStencilView(depthStencilPtr, &descDSV, &this->DepthStencilViewPtr);
		ErrorIf(FAILED(depthStencilResult), "Depth stencil creation failed");
	}

	void GraphicsManager::_CreateAndBindIndexBuffer()
	{
		if (this->_RunningIndexVector.size() <= 0)
		{
			ErrorIf(true, "Index buffer creation attempted with empty index list");
		}

		D3D10_BUFFER_DESC bufferDescription;
		bufferDescription.Usage = D3D10_USAGE_DEFAULT;
		bufferDescription.ByteWidth = sizeof(UINT) * this->_RunningIndexVector.size();
		bufferDescription.BindFlags = D3D10_BIND_INDEX_BUFFER;
		bufferDescription.CPUAccessFlags = 0;
		bufferDescription.MiscFlags = 0;

		this->IndexData.pSysMem = &this->_RunningIndexVector[0];

		HRESULT indexBufferCreatedResult = 
			this->DevicePtr->CreateBuffer(&bufferDescription, &this->IndexData, &this->IndexBufferPtr);

		ErrorIf(FAILED(indexBufferCreatedResult), "Index buffer creation failed");

		// Set index buffer
		this->DevicePtr->IASetIndexBuffer(this->IndexBufferPtr, DXGI_FORMAT_R32_UINT, 0);
	}

	void GraphicsManager::_RegisterObjectTexture(string filePath, unsigned int* outTextureID)
	{
		ID3D10ShaderResourceView* texture;
		HRESULT result = D3DX10CreateShaderResourceViewFromFile(this->DevicePtr, filePath.c_str(), NULL, NULL, &texture, NULL);

		this->_RegisteredTextureVector.push_back(texture);

		ErrorIf(FAILED(result), "Texture creation failed");

		// Assign id to the texture
		*outTextureID = TextureIDCounter++;
	}
}
