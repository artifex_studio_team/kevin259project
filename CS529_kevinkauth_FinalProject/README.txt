/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology. Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: README.txt
Purpose: Required readme file
Language: N/A
Platform: N/A
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 10/24/2012
- End Header --------------------------------------------------------*/

The object of the game is to knock as many of the enemy cubes out of the arena
without getting knocked out yourself.

Breakdown of syllibus components:
+60 Start

+0, 60	Basic Debug Drawing: 
		Turned on by default.  Represented byarrows drawn 
		to represent the velocity of moving objects.
+2, 62	Advanced Debug Drawing:
		Turned on by default.  Represented by printing of frame rate
		in top left corner.
+0, 62  Basic c++ Objects:
		Theyre everywhere...
+4, 68	Component based design:
		Two components for each object, Physics and Drawing.  The code for
		which can be found in the
		SceneManager->GameobjectManager->GameObjects->Components
		directory.
+2, 70	Some form of object management:
		GameObjectManager.h/ GameObjectManagement.cpp.
		Allocates more than enough memory at the start of a scene, keeps track of
		live vs dead objects.  Replaces dead objects upon new object creation.
+0, 70	Basic Events:
		Key presses control the avatar...
+0, 70 	Function Binding:
		Not included.
+0, 70	Signals/ Slots:
		Not included.
+0, 70	Basic text serialization:
		Used to save and load the game.  Saved games can be found in GameSave.txt.
		Click Save & Quit from main menu/ Load last save to test.
+0, 70	Creation of objects from data files:
		Not included.
+0, 70 Advanced serialization:
		Not included.
+0, 70 Level files:
		Not included.
+0, 70 Archetypes:
		Not included.
+0, 70 Level editor:
		Not included.
+0, 70 Textured Sprite Rendering:
		6 different textured objects running around...
+0, 70 Sprite Animations:
		Not included.
+2, 72 Sprite Z sorting:
		3D game, uses a Z buffer
+2, 74 Sprite Batching:
		Messages are used to send draw calls to the graphics manager,
		which draws them all at once upon update.  Check GraphicsManager.cpp/.h
		Update(...) method to see implementation.
+0, 74 Other advanced graphics features:
		Not included.
+0, 74 Basic collision Detection:
		All over the place...  Code is in PhysicsComponent.h/.cpp
+0, 74 Basic collision response:
		All over the place...  Code is in PhysicsComponent.h/.cpp
+2, 76 Impulse based collision response:
		Notice transfer of energy between objects based on speed.  Code
		is in PhysicsComponent.h/.cpp
+0, 76 Advanced physics features:
		None included.
+0, 76 Human control of ship:
		Obvious...
+0, 76 30fps:
		Confirmed by the frame rate printed on screen
+0, 76 Enemy Types:
		2 types; Enemy that homes in on you, and a static enemy that shoots
		bullets at you.
+0, 76 Weapon/Powerups:
		2 weapons: Bullets and homing missiles
+0, 76 Controls screen:
		Located at the bottom of the main menu
+2, 78 Menus:
		Main menu used to select between various options, boots up
		on startup and upon hitting escape key during gameplay.
+0, 78 Win/Loose condition:
		Run out of lives, you loose.  Knock all the enemies out, you win.
+4, 82 Game is fun for a couple minutes:
		I think so.  You have to get close to the edge to get the other cubes
		to go over.  Kinda fun for a couple minutes...
+0, 82 Crashes/ Soft locks:
		Fingers crossed...
+2, 84 Other features:
		I implemented a message logging system to help debug the messaging
		system, and to enable a developer to send a specialized print
		to log file message to help with general debugging in a way
		that isn't forced to be printed to screen.  The code is in LoggingManager.h/.cpp
		which is a cors system that gets sent all bradcasts messages and
		outputs a description of them to log.txt.  The base message object
		was modified to force all inheritors to implements a GetDescription()
		function used by the logging manager for this purpose.  

Grade: 84






	