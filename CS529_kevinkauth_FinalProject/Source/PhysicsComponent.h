/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: PhysicsComponent.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "GameObjectComponent.h"
#include "Precompiled.h"
#include "WindowsSystem.h"
#include "Message.h"
#include "VectorMath.h"
#include "LineSegment.h"

namespace Framework
{
	class PhysicsComponent : public GameObjectComponent
	{
	public:
		PhysicsComponent(){};
		~PhysicsComponent(){};

		// Static methods
		static bool AreColliding(GameObject* obj1, GameObject* obj2);
		static void BounceObjectsOffEachOtherIfColliding(GameObject* obj1, GameObject* obj2);
		static void HomeObjectOnTarget(GameObject* obj1, GameObject* target);
		static D3DXVECTOR3 RotationYToVelocity(float rotationY, float scale);
		static float VelocityToRotationY(D3DXVECTOR3 velocity);

		// Non static methods
		virtual void Initialize();
		virtual void RecieveMessage(Message* message);
		void UpdatePosition(float timeSlice);

	private:
		float MovementAccelaration;
		float BrakeStrengthPerFrame;
		float TurningVelocity;
		float SpeedLimit;
		bool AccelarateBackwardsOn;
		bool AccelarateForewardsOn;
		bool TurnRightOn;
		bool TurnLeftOn;

		void _Accelarate(bool foreward, float timeSlice);
		void _Turn(bool right, float timeSlice);
	};
}