/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: GraphicsManager.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once //Makes sure this header is only included once

#include "DebugDiagnostic.h"
#include "Camera.h"
#include "EngineVertex.h"
#include "System.h"
#include <sstream>
#include <vector>

using namespace std;

namespace Framework
{	
	struct EffectGroup
	{
		EffectGroup::EffectGroup(ID3D10Effect* effect)
		{
			this->EffectPtr = effect;
			this->EffectTechniquePtr = this->EffectPtr->GetTechniqueByIndex(0);
			worldMatrixVariablePtr = EffectPtr->GetVariableByName( "World" )->AsMatrix();
			viewMatrixVariablePtr = EffectPtr->GetVariableByName( "View" )->AsMatrix();
			projectionMatrixVariablePtr = EffectPtr->GetVariableByName( "Projection" )->AsMatrix();
		}

		void SetTranformationMatricies(D3DXMATRIX* worldMatrix, D3DXMATRIX* viewMatrix, D3DXMATRIX* projectionMatrix)
		{
			this->worldMatrixVariablePtr->SetMatrix((float*)worldMatrix);
			this->viewMatrixVariablePtr->SetMatrix((float*)viewMatrix);
			this->projectionMatrixVariablePtr->SetMatrix((float*)projectionMatrix);
		}

		ID3D10Effect* EffectPtr;
		ID3D10EffectTechnique* EffectTechniquePtr;
		ID3D10EffectMatrixVariable* worldMatrixVariablePtr;
		ID3D10EffectMatrixVariable* viewMatrixVariablePtr;
		ID3D10EffectMatrixVariable* projectionMatrixVariablePtr;
	};

	struct ObjectMesh
	{
		std::vector<SimpleVertex> VertexVector;
		std::vector<unsigned int> IndexVector;
		unsigned int VertexBufferOffset;
		unsigned int IndexBufferOffset;
		D3D10_PRIMITIVE_TOPOLOGY Topology;
		unsigned int TextureID;
	};

	class GraphicsManager : public ISystem
	{
	public:
		GraphicsManager(HWND hWnd,int screenWidth,int screenHeight);
		~GraphicsManager();	

		void Update(float dt);

		void RecieveMessage(Message* message);

		virtual std::string GetName(){return "GraphicsManager";}
			

	private:
		// Fields
		float frameRate;
		IDXGISwapChain* SwapChainPtr;
		D3D10_VIEWPORT ViewPort;
		D3D10_SUBRESOURCE_DATA IndexData; 
		ID3D10Buffer* IndexBufferPtr;
		ID3D10Device* DevicePtr;
		ID3D10RenderTargetView* RenderTargetViewPtr;
		ID3D10DepthStencilView* DepthStencilViewPtr;
		LPD3DX10SPRITE DebugTextSprite;
		LPD3DX10FONT DebugWritingFont;
		EffectGroup* CurrentEffectGroupPtr;
		Camera* _CameraPtr;
		std::vector<SimpleVertex> _RunningVertexVector;
		std::vector<unsigned int> _RunningIndexVector;
		std::vector<ObjectMesh*> _RegisteredMeshVector;
		std::vector<MessageDrawObject> _PendingDrawObjectMessages;
		std::vector<MessageDrawText> _PendingDrawTextMessages;
		D3DXMATRIX _ProjectionMatrix;
		HWND HWnd;
		int ScreenWidth;
		int ScreenHeight;
		vector<ID3D10ShaderResourceView*> _RegisteredTextureVector;

		// Methods
		void Initialize();
		void _CreateEffectGroup(EffectGroup** effectGroupReference, char* fileName);
		void _CreateAndBindVertexBuffer();
		void _CreateAndBindIndexBuffer();
		void _CreateDepthStencilBuffer();
		void _DrawObject(D3DXMATRIX* objectWorldMatrixPtr, D3DXMATRIX* viewMatrixPtr, unsigned int objectMeshID);
		void _DrawText(std::string text, RECT locationRect, D3DXVECTOR4 fontColor);
		void _SetPipelineMatrices(D3DXMATRIX* worldMatrixPtr, D3DXMATRIX* viewMatrixPtr);
		void _RegisterObjectMesh(
			std::vector<SimpleVertex>* objectVerteciesPtr,
			std::vector<unsigned int>* objectDrawIndeciesPtr,
			string textureFilePath,
			D3D10_PRIMITIVE_TOPOLOGY topology,
			unsigned int* outMeshIDPtr);
		void _RegisterObjectTexture(string filePath, unsigned int* outTextureID);
		void _UnloadAllRegisteredMesh();	
	};
}
