/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: DrawingComponent.cpp
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "DrawingComponent.h"
#include "GameObject.h"

namespace Framework
{
	static const string DEBUG_ARROW_TEXTURE_FILE_PATH = "ArrowTexture.png";
	static unsigned int DebugArrowVelocityMeshID;

	DrawingComponent::DrawingComponent(UINT objectMeshID)
	{
		this->_MeshID = objectMeshID;

		if (DEBUG_DRAWING && DebugArrowVelocityMeshID == 0)
		{
			_RegisterDebugArrowMesh(D3DXVECTOR4(0.1f, 0.1f, 1.0f, 1.0f), &DebugArrowVelocityMeshID);
		}
	}

	DrawingComponent::~DrawingComponent()
	{
	}

	void DrawingComponent::RecieveMessage(Message* message)
	{
	}

	void DrawingComponent::Initialize()
	{
		// World matrix as identity
		this->_WorldMatrix = D3DXMATRIX
			(
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			);
	}

	void DrawingComponent::DrawObject(Camera* cameraPtr)
	{
		// Prepare world matrix
		this->_WorldMatrix = 
			D3DXMATRIX
			(
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			);

		// Rotate X
		D3DXMATRIX rotationMatrixX = 
			D3DXMATRIX
			(
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, cos(this->GetOwner()->RotationX), sin(this->GetOwner()->RotationX), 0.0f,
				0.0f, -sin(this->GetOwner()->RotationX), cos(this->GetOwner()->RotationX), 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			);

		D3DXMATRIX rotationMatrixY = 
			D3DXMATRIX
			(
				cos(*this->GetOwner()->GetRotationY()), 0.0f, sin(*this->GetOwner()->GetRotationY()), 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				-sin(*this->GetOwner()->GetRotationY()), 0.0f, cos(*this->GetOwner()->GetRotationY()), 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			);

		D3DXMATRIX rotationMatrixZ = 
			D3DXMATRIX
			(
				cos(this->GetOwner()->RotationZ), sin(this->GetOwner()->RotationZ), 0.0f, 0.0f,
				-sin(this->GetOwner()->RotationZ), cos(this->GetOwner()->RotationZ), 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			);

		D3DXMATRIX scaleMatrix = 
			D3DXMATRIX
			(
				this->GetOwner()->Scale, 0.0f, 0.0f, 0.0f,
				0.0f, this->GetOwner()->Scale, 0.0f, 0.0f,
				0.0f, 0.0f, this->GetOwner()->Scale, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			);

		D3DXMATRIX translationMatrix = 
			D3DXMATRIX
			(
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				this->GetOwner()->Position.x, this->GetOwner()->Position.y, this->GetOwner()->Position.z, 1.0f
			);

		this->_WorldMatrix = scaleMatrix * rotationMatrixX * rotationMatrixY * rotationMatrixZ * translationMatrix;

 		SYSTEM_MANAGER->BroadcastMessage(&MessageDrawObject(&this->_WorldMatrix, cameraPtr->GetViewMatrix(), this->_MeshID));

		if (DEBUG_DRAWING)
		{
			this->_DrawDebugArrows(cameraPtr);
		}
	}

	void DrawingComponent::_RegisterDebugArrowMesh(D3DXVECTOR4 arrowColor, unsigned int* outMeshID)
	{
		std::vector<SimpleVertex> vertices;
		vertices.push_back(SimpleVertex(D3DXVECTOR4(0.0f, 0.0f, 0.0f, 1.0f), D3DXVECTOR2(0.0f, 0.0f)));
		vertices.push_back(SimpleVertex(D3DXVECTOR4(1.0f, 0.0f, 0.0f, 1.0f), D3DXVECTOR2(0.0f, 0.0f)));

		vertices.push_back(SimpleVertex(D3DXVECTOR4(1.0f, 0.0f, 0.0f, 1.0f), D3DXVECTOR2(0.0f, 0.0f)));
		vertices.push_back(SimpleVertex(D3DXVECTOR4(0.75f, 0.0f, 0.25f, 1.0f), D3DXVECTOR2(0.0f, 0.0f)));

		vertices.push_back(SimpleVertex(D3DXVECTOR4(1.0f, 0.0f, 0.0f, 1.0f), D3DXVECTOR2(0.0f, 0.0f)));
		vertices.push_back(SimpleVertex(D3DXVECTOR4(0.75f, 0.0f, -0.25f, 1.0f), D3DXVECTOR2(0.0f, 0.0f)));

		std::vector<unsigned int> indices;
		indices.push_back(0), indices.push_back(1);
		indices.push_back(2), indices.push_back(3);
		indices.push_back(4), indices.push_back(5);

		SYSTEM_MANAGER->BroadcastMessage(
			&MessageRegisterObjectMesh(
				&vertices,
				&indices,
				D3D10_PRIMITIVE_TOPOLOGY_LINELIST,
				DEBUG_ARROW_TEXTURE_FILE_PATH,
				outMeshID)
		);
	}

	void DrawingComponent::_DrawDebugArrows(Camera* cameraPtr)
	{
		// Rotate X
		D3DXVECTOR3 ownerVelocity = *this->GetOwner()->GetVelocity();
		float length = VectorMath::VectorLength(&ownerVelocity);

		D3DXMATRIX rotationMatrixY = 
			D3DXMATRIX
			(
				ownerVelocity.x, 0.0f, ownerVelocity.z, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				-ownerVelocity.z, 0.0f, ownerVelocity.x, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			);

		D3DXMATRIX scaleMatrix = 
			D3DXMATRIX
			(
				length, 0.0f, 0.0f, 0.0f,
				0.0f, length, 0.0f, 0.0f,
				0.0f, 0.0f, length, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			);

		D3DXMATRIX translationMatrix = 
			D3DXMATRIX
			(
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				this->GetOwner()->Position.x, this->GetOwner()->Position.y, this->GetOwner()->Position.z, 1.0f
			);

		this->_WorldMatrixDebug = scaleMatrix * rotationMatrixY * translationMatrix;

		SYSTEM_MANAGER->BroadcastMessage(
			&MessageDrawObject(&this->_WorldMatrixDebug, cameraPtr->GetViewMatrix(), DebugArrowVelocityMeshID));
	}
}