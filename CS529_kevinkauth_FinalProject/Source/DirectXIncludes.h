/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: DirectXIncludes.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#if defined(_DEBUG)
#include <DxErr.h>
#ifndef D3D_DEBUG_INFO
#define D3D_DEBUG_INFO
#endif
#endif

#if defined(_DEBUG)
#ifndef DXVerify
#define DXVerify(exp) { HRESULT hr = (exp); if( FAILED(hr) && SignalErrorHandler(#exp, __FILE__, __LINE__, "DX Error %s", DXGetErrorDescription(hr) ) ) { G_DEBUG_BREAK; } }
#endif
#else
#ifndef DXVerify
#define DXVerify(x) (x)
#endif
#endif

#include "DXGI.h"
#include "D3Dcommon.h"
#include <D3D10.h>
#include <D3DX10.h>
#include "D3Dcompiler.h"

