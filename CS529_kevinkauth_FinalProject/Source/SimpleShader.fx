matrix World;
matrix View;
matrix Projection;
Texture2D txDiffuse;

SamplerState samLinear
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
};

struct VS_OUTPUT
{
    float4 Pos : SV_POSITION;
	float2 TexCoord: TEXCOORD;
};

VS_OUTPUT VS( float4 pos : POSITION, float2 texCoord  : TEXCOORD)
{
    VS_OUTPUT output = (VS_OUTPUT)0;
    output.Pos = mul( pos, World );
    output.Pos = mul( output.Pos, View );
    output.Pos = mul( output.Pos, Projection );
	output.TexCoord = texCoord;
	return output;
}

float4 PS(VS_OUTPUT vsOutput) : SV_Target
{
	return txDiffuse.Sample(samLinear, vsOutput.TexCoord);
}

technique10 Render
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, VS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, PS()));
	}
}