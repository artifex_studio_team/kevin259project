/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: WindowsSystem.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once //Makes sure this header is only included once

#include "System.h"
#include <sstream>

namespace Framework
{
	///Basic manager for windows. Implements the windows message pump and
	///broadcasts user input messages to all the systems.
	class WindowsSystem : public ISystem
	{
	public:
		WindowsSystem(const char* windowTitle, int ClientWidth, int ClientHeight);	//The constructor
		~WindowsSystem();															//The destructor

		void ActivateWindow();								//Activate the game window so it is actually visible
		virtual void Update(float dt);						//Update the system every frame
		virtual std::string GetName() {return "Windows";}	//Get the string name of the system

		HWND hWnd;											//The handle to the game window
		HINSTANCE hInstance;								//The handle to the instance
		POINTS MousePosition;
	};

	///Message signaling that a key is pressed.
	class MessageCharacterKey : public Message
	{
	public:
		MessageCharacterKey() : Message(Mid::CharacterKey) {};	
		std::string GetDescription() { return "Key Press: " + character; };
		int character;
	};

	// Key down message
	class MessageKeyDown : public Message
	{
	public:
		int Character;
		MessageKeyDown(int character) : Message(Mid::KeyDown) 
		{
			this->Character = character;
		};

		std::string GetDescription() { return "Key Down: " + this->Character; };
	};

	class MessageKeyUp : public Message
	{
	public:
		int Character;
		MessageKeyUp(int character) : Message(Mid::KeyUp) 
		{
			this->Character = character;
		};

		std::string GetDescription() { return "Key Up: " + this->Character; };
	};

	///Message signaling that a mouse button state has changed.
	class MouseButton: public Message
	{
	public:
		enum MouseButtonIndexId
		{
			LeftMouse,
			RightMouse
		};
		MouseButton(MouseButtonIndexId button, bool state, D3DXVECTOR2 position) 
			: Message(Mid::MouseButton) ,  MouseButtonIndex(button) , ButtonIsPressed(state), MousePosition(position) {};

		std::string GetDescription() { return "Mouse button pressed"; };
		MouseButtonIndexId MouseButtonIndex;
		bool ButtonIsPressed;
		D3DXVECTOR2 MousePosition;
	};

	///Message signaling that the mouse has moved.
	class MouseMove: public Message
	{
	public:
		D3DXVECTOR2 MousePosition;
		MouseMove(D3DXVECTOR2 position) : Message(Mid::MouseMove) , MousePosition(position) {};	
		std::string GetDescription() 
		{ 
			std::stringstream ss;
			ss << "Mouse Move " << MousePosition.x << "," <<(float)MousePosition.y;
			std::string returnString;
			ss >> returnString;
			return returnString; 
		};
		
	};

	///Message signaling that a file was dropped onto the window.
	class FileDrop: public Message
	{
	public:
		FileDrop(std::string filename) : Message(Mid::FileDrop) , FileName(filename) {};	
		std::string FileName;
		std::string GetDescription()
		{
			return "File Drop Event: " + this->FileName;
		}
	};

	bool IsShiftHeld();
	bool IsCtrlHeld();
	bool IsAltHeld();
	bool IsUpHeld();
	bool IsDownHeld();
	bool IsLeftHeld();
	bool IsRightHeld();

	extern WindowsSystem* WINDOWSSYSTEM;
}