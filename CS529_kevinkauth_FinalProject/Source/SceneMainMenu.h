/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: SceneMainMenu.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "Scene.h"
#include "Message.h"

namespace Framework
{
	class SceneMainMenu : public Scene
	{
	public:
		SceneMainMenu();
		~SceneMainMenu();
		virtual void SceneLoad();
		virtual void SceneInit();
		virtual void SceneUpdate(float timeSlice);
		virtual void SceneDraw();
		virtual void SceneFree();
		virtual void SceneUnload();
		virtual void RecieveMessage(Message* message);
	private:
		MessageDrawText* _OptionsTitle;
		MessageDrawText* _NewGameMessage;
		MessageDrawText* _ContinueGameMessage;
		MessageDrawText* _LoadGameMessage;
		//MessageDrawText* _OptionsMessage;
		MessageDrawText* _QuitMessage;
		MessageDrawText* _ControlsTitle;
		MessageDrawText* _ForwardControl;
		MessageDrawText* _LeftControl;
		MessageDrawText* _RightControl;
		MessageDrawText* _BrakeControl;
		MessageDrawText* _BulletControl;
		MessageDrawText* _MissileControl;
		MessageDrawText* _MainMenuControl;

		bool _IsCursorOverMenuItem(MessageDrawText* menuItem, D3DXVECTOR2 cursorPosition);
	};
}