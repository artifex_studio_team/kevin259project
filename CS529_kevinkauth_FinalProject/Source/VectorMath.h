/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: VectorMath.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "Precompiled.h"
#include "DirectXIncludes.h"

namespace Framework
{
	class VectorMath
	{
	public:
		static void ScaleVector(const D3DXVECTOR3* vectorIn, D3DXVECTOR3* const vectorOut, float scale);
		static float VectorLength(const D3DXVECTOR3* vector);
		static float SmallestAngleBetweenXZPlane(D3DXVECTOR3 const* vector1, D3DXVECTOR3 const* vector2);
		static void GetXZNormal(const D3DXVECTOR3* vectorIn, D3DXVECTOR3* normalOut);
		static void Normalize(const D3DXVECTOR3* vectorIn, D3DXVECTOR3* vectorOut);
		static float DistanceBetweenPoints(const D3DXVECTOR3* pos1, const D3DXVECTOR3* pos2);
	};
}