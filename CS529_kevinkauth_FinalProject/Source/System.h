/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: System.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once

#include "Message.h"

namespace Framework
{
	///System is a pure virtual base class (which is to say, an interface) that is
	///the base class for all systems used by the game. 
	class ISystem
	{
	public:
		///Systems can receive all message send to the Core. 
		///See Message.h for details.
		virtual void RecieveMessage(Message* message) {};

		///All systems are updated every game frame.
		virtual void Update(float timeslice) = 0;	

		///All systems provide a string name for debugging.
		virtual std::string GetName() = 0;	

		///Initialize the system.
		virtual void Initialize(){};
		
		///All systems need a virtual destructor to have their destructor called 
		virtual ~ISystem(){}						
	};
}
