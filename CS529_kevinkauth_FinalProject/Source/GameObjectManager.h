/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: GameObjectManager.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "System.h"
#include "GameObject.h"

namespace Framework
{
	class GameObjectManager
	{
	public:
		GameObject** ObjectArray;
		unsigned int HighestLiveIndex;

		GameObjectManager();
		GameObjectManager(std::ifstream* reader);
		virtual ~GameObjectManager();

		// Rrturns index of created object in array
		GameObject* CreateObject(unsigned int meshID, bool isStatic);
		void DrawAllLiveObjects(Camera* cameraPtr);
		void DestroyAllObjects();
		std::string ToString();
	};
}
