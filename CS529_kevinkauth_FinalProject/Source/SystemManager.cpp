/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: SystemManager.cpp
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "SystemManager.h"

namespace Framework
{

	//A global pointer to the core
	SystemManager* SYSTEM_MANAGER;

	SystemManager::SystemManager()
	{
		LastTime = 0;
		GameActive = true;
		SYSTEM_MANAGER = this; //Set the global pointer
	}

	SystemManager::~SystemManager()
	{
		//Nothing for the destructor to do
	}

	void SystemManager::Initialize()
	{
		for (unsigned i = 0; i < Systems.size(); ++i)
			Systems[i]->Initialize();
	}

	void SystemManager::GameLoop()
	{
		//Initialize the last time variable so our first frame
		//is "zero" seconds (and not some huge unknown number)
		LastTime = timeGetTime();

		while (GameActive)
		{
			//Get the current time in milliseconds
			unsigned currenttime = timeGetTime();
			//Convert it to the time passed since the last frame (in seconds)
			float dt = (currenttime - LastTime) / 1000.0f;
			//Update the when the last update started
			LastTime = currenttime;

			//Update every system and tell each one how much
			//time has passed since the last update
			for (unsigned i = 0; i < Systems.size(); ++i)
			{
				if (!GameActive)
				{
					// In case we quit mid update cycle
					break;
				}
				Systems[i]->Update(dt);
			}
		}

	}

	void SystemManager::BroadcastMessage(Message* message)
	{
		//The message that tells the game to quit
		if (message->MessageId == Mid::Quit)
			GameActive = false;

		//Send the message to every system--each
		//system can figure out whether it cares
		//about a given message or not
		for (unsigned i = 0; i < Systems.size(); ++i)
			Systems[i]->RecieveMessage(message);
	}

	void SystemManager::AddSystem(ISystem* system)
	{
		//Add a system to the core to be updated
		//every frame
		Systems.push_back(system);
	}

	void SystemManager::DestroySystemsAndQuit()
	{			
		//Delete all the systems in reverse order
		//that they were added in (to minimize any
		//dependency problems between systems)
		for (unsigned i = 0; i < Systems.size(); ++i)
		{
			delete Systems[Systems.size()-i-1];
		}

		this->GameActive = false;
	}

}
