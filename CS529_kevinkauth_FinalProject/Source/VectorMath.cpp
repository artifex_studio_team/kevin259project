/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: VectorMath.cpp
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#include "Precompiled.h"
#include "VectorMath.h"

namespace Framework
{
	float VectorMath::DistanceBetweenPoints(const D3DXVECTOR3* pos1, const D3DXVECTOR3* pos2)
	{
		D3DXVECTOR3 from1To2 = *pos2 - *pos1;
		return VectorMath::VectorLength(&from1To2);
	}

	void VectorMath::Normalize(const D3DXVECTOR3* vectorIn, D3DXVECTOR3* vectorOut)
	{
		float length = VectorMath::VectorLength(vectorIn);

		if (length > 0)
		{
			float scale = 1.0f / length;
			VectorMath::ScaleVector(vectorIn, vectorOut, scale);
		}
		else
		{
			*vectorOut = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		}
	}

	void VectorMath::ScaleVector(const D3DXVECTOR3* vectorIn, D3DXVECTOR3* vectorOut, float scale)
	{
			vectorOut->x = vectorIn->x * scale;
			vectorOut->y = vectorIn->y * scale;
			vectorOut->z = vectorIn->z * scale;
	}

	float VectorMath::VectorLength(const D3DXVECTOR3* vector)
	{
		return sqrt(sqrt(vector->x * vector->x + vector->y * vector->y) *
			sqrt(vector->x * vector->x + vector->y * vector->y) +
			(vector->z * vector->z));
	}

	float VectorMath::SmallestAngleBetweenXZPlane(D3DXVECTOR3 const* vector1, D3DXVECTOR3 const* vector2)
	{
		float numerator = (vector1->x * vector2->x) + (vector1->z * vector2->z);
		float denominator = VectorMath::VectorLength(vector1) * VectorMath::VectorLength(vector2);
		return acos(numerator / denominator);
	}

	void VectorMath::GetXZNormal(const D3DXVECTOR3* vectorIn, D3DXVECTOR3* normalOut)
	{
		// Flip X and Z, invert the Z
		normalOut->x = vectorIn->z;
		normalOut->y = vectorIn->y;
		normalOut->z = -vectorIn->x;
	}
}