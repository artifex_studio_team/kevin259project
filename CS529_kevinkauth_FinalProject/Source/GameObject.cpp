/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: GameObject.cpp
Purpose: Code for base game objects
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "Precompiled.h"
#include "GameObject.h"
#include "GameObjectManager.h"
#include "DrawingComponent.h"
#include "PhysicsComponent.h"
#include <algorithm>

using namespace std;

namespace Framework
{
	static const float SPEED_CAP_DEFAULT = 25.0f;
	static const float SPEED_CAP_PROJECTILE = 50.0f;
	static unsigned int idCounter = 0;

	//Used to sort components using their type Id.
	struct ComponentSorter
	{
		bool operator()(GameObjectComponent* left, GameObjectComponent* right)const
		{
			return left->TypeId < right->TypeId;
		}
	};

	//Binary search a sorted array of components.
	GameObjectComponent* BinaryCompoentSearch(ComponentArray& components, ComponentTypeId name)
	{
		size_t begin = 0;
		size_t end = components.size();

		while(begin < end)
		{
			size_t mid = (begin+end) / 2;
			if(components[mid]->TypeId < name)
				begin = mid + 1;
			else
				end = mid;
		}

		if((begin < components.size()) && (components[begin]->TypeId == name))
			return components[begin];
		else
			return NULL;
	}

	void GameObject::Initialize(unsigned int type, bool isStatic)
	{
		this->ObjectId = idCounter++;
		this->IsStatic = isStatic;
		this->Type = type;
		this->LifeState = ALIVE;
		this->Position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		this->Velocity = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
		this->RotationX = 0.0f;
		this->RotationY = 0.0f;
		this->RotationZ = 0.0f;
		this->Scale = 1.0f;

		this->AddComponent(CT_3DDrawing, new DrawingComponent(type));
		this->AddComponent(CT_Physics, new PhysicsComponent());

		for(ComponentIt it = this->Components.begin();it!=Components.end();++it)
		{	
			(*it)->Base = this;
			(*it)->Initialize();
		}
	}

	GameObject::GameObject()
	{
		this->ObjectId = 0;
		this->LifeState = DEAD;
	}

	GameObject::GameObject(ifstream* reader)
	{
		// Load from string
		string nextLine;
		getline(*reader, nextLine);

		ErrorIf(nextLine.compare("<Object>"), "Invalid stream used to load object");

		// These we know for sure
		this->LifeState = ALIVE;
		this->ObjectId = idCounter++;

		// Is static
		getline(*reader, nextLine);
		this->IsStatic = (bool)atoi(nextLine.c_str());

		// Load position
		getline(*reader, nextLine);
		float posX = (float)atof(nextLine.c_str());

		getline(*reader, nextLine);
		float posY = (float)atof(nextLine.c_str());

		getline(*reader, nextLine);
		float posZ = (float)atof(nextLine.c_str());

		this->Position = D3DXVECTOR3(posX, posY, posZ);

		// Rotation X
		getline(*reader, nextLine);
		this->RotationX = (float)atof(nextLine.c_str());

		// Rotation Y
		getline(*reader, nextLine);
		this->RotationY = (float)atof(nextLine.c_str());

		// Rotation Z
		getline(*reader, nextLine);
		this->RotationZ = (float)atof(nextLine.c_str());

		// Scale
		getline(*reader, nextLine);
		this->Scale = (float)atof(nextLine.c_str());

		// Type / mesh id
		getline(*reader, nextLine);
		this->Type = atoi(nextLine.c_str());

		// Load velocity
		getline(*reader, nextLine);
		float velX = (float)atof(nextLine.c_str());

		getline(*reader, nextLine);
		float velY = (float)atof(nextLine.c_str());

		getline(*reader, nextLine);
		float velZ = (float)atof(nextLine.c_str());

		this->Velocity = D3DXVECTOR3(velX, velY, velZ);

		// Should be at the end of the section now
		getline(*reader, nextLine);
		ErrorIf(nextLine.compare("</Object>"), "Invalid stream used to load object");

		// Create and initialize components
		this->AddComponent(CT_3DDrawing, new DrawingComponent(this->Type));
		this->AddComponent(CT_Physics, new PhysicsComponent());

		for(ComponentIt it = Components.begin();it!=Components.end();++it)
		{	
			(*it)->Base = this;
			(*it)->Initialize();
		}
	}

	GameObject::~GameObject()
	{
		//Delete each component using the component's virtual destructor
		//takes care of all resources and memory.
		for(ComponentIt it = Components.begin();it!=Components.end();++it)
			delete *it;
	}

	void GameObject::AddComponent(ComponentTypeId typeId, GameObjectComponent* component)
	{
		//Store the components type Id
		component->TypeId = typeId;
		Components.push_back(component);

		//Sort the component array so binary search can be used to find components quickly.
		std::sort(Components.begin(), Components.end(), ComponentSorter() );
	}

	const D3DXVECTOR3* GameObject::GetVelocity()
	{
		return &this->Velocity;
	}

	void GameObject::SetVelocity(D3DXVECTOR3 velocity)
	{
		float speed = VectorMath::VectorLength(&velocity);
		if (this->Type == 1 || this->Type == 2) // Hack alert
		{
			if (speed > SPEED_CAP_PROJECTILE)
			{
				VectorMath::Normalize(&velocity, &velocity);
				VectorMath::ScaleVector(&velocity, &velocity, SPEED_CAP_PROJECTILE);
			}
		}
		else if (speed > SPEED_CAP_DEFAULT)
		{
			VectorMath::Normalize(&velocity, &velocity);
			VectorMath::ScaleVector(&velocity, &velocity, SPEED_CAP_DEFAULT);
		}

		this->Velocity = velocity;
		this->RotationY = PhysicsComponent::VelocityToRotationY(velocity);
	}

	const float* GameObject::GetRotationY()
	{
		return &this->RotationY;
	}

	void GameObject::SetRotationY(float rotationY)
	{
		this->RotationY = rotationY;
		float scale = VectorMath::VectorLength(&this->Velocity);
		this->Velocity = PhysicsComponent::RotationYToVelocity(rotationY, scale);
	}

	void GameObject::Draw(Camera* cameraPtr)
	{
		DrawingComponent* drawComponentPtr;
		if (drawComponentPtr = this->GetComponetType<DrawingComponent>(CT_3DDrawing))
		{
			drawComponentPtr->DrawObject(cameraPtr);
		}
	}

	void GameObject::UpdatePosition(float timeSlice)
	{
		PhysicsComponent* physicsComponentPtr;
		if (physicsComponentPtr = this->GetComponetType<PhysicsComponent>(CT_Physics))
		{
			physicsComponentPtr->UpdatePosition(timeSlice);
		}
	}

	void GameObject::RecieveMessage(Message * message)
	{
		//When a message is sent to a composition it sends to
		//all of its components
		//for( MapIt it = Map.begin();it!=Map.end();++it)
		if (this->LifeState == ALIVE)
		{
			for(ComponentIt it = Components.begin();it!=Components.end();++it)
				(*it)->RecieveMessage(message);
		}
	}

	GameObjectComponent* GameObject::GetComponent(ComponentTypeId typeId)
	{
		return BinaryCompoentSearch(Components, typeId);
	}

	void GameObject::Destroy()
	{ 
		this->LifeState = DEAD;
		
		// Delete all components
		for (unsigned int i = 0; i < this->Components.size(); ++i)
		{
			if (this->Components[i])
			{
				delete this->Components[i];
			}
		}

		this->Components.clear();
	}

	std::string GameObject::ToString()
	{
		std::stringstream builder;

		builder << "<Object>" << '\n';

		// Write stuff, alphabetical order of variable name
		builder << this->IsStatic << '\n';
		builder << this->Position.x << '\n';
		builder << this->Position.y << '\n';
		builder << this->Position.z << '\n';
		builder << this->RotationX << '\n';
		builder << this->RotationY << '\n';
		builder << this->RotationZ << '\n';
		builder << this->Scale << '\n';
		builder << this->Type << '\n';
		builder << this->Velocity.x << '\n';
		builder << this->Velocity.y << '\n';
		builder << this->Velocity.z << '\n';

		builder << "</Object>" << '\n';

		return builder.str();
	}
}
