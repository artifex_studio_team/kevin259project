/* Start Header -------------------------------------------------------
Copyright (C) 2012 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written consent of DigiPen Institute of Technology is prohibited.
File Name: GameObject.h
Purpose: Is code
CS 529 � Fundamentals of Game Development
� Copyright DigiPen Institute of Technology. All rights reserved. 7
Language: C++ Visual Studio 2010 default compiler
Platform: Windows 7, DirectX 10
Project: CS529_kevinkauth_FinalProject
Author: Kevin Kauth, kevin.kauth, 60002211
Creation date: 12/12/2012
- End Header --------------------------------------------------------*/

#pragma once
#include "Message.h"
#include "GameObjectComponent.h"
#include <string>
#include <fstream>
#include <iostream>
#include "Camera.h"

namespace Framework
{

	///Game Object Id Type
	typedef unsigned int GOCId;

	//A vector of components pointers is used to store components.
	typedef std::vector<GameObjectComponent*> ComponentArray;

	enum GameObjectLifeState
	{
		ALIVE = 0,
		DEAD
	};

	class GameObject
	{
	public:
		friend class GameObjectManager;
		
		GameObjectLifeState LifeState;
		D3DXVECTOR3 Position;
		
		float RotationX;
		float RotationZ;
		float Scale;
		unsigned int Type;
		bool IsStatic;

		void RecieveMessage(Message* message);
		GameObjectComponent* GetComponent(ComponentTypeId typeId);

		// Type safe way of accessing components.
		template<typename type>
		type* GetComponetType(ComponentTypeId typeId);
		void Initialize(unsigned int type, bool IsStatic);
		void Destroy();
		void Draw(Camera* cameraPtr);
		void UpdatePosition(float timeSlice);
		std::string ToString();

		void AddComponent(ComponentTypeId typeId, GameObjectComponent * component);
		D3DXVECTOR4 GetHeadingXZPlane()
		{
			D3DXVECTOR4 result;
			result.y = 0;
			result.x = cos(this->RotationY);
			result.z = sin(this->RotationY);
			return result;
		}

		GOCId GetId(){return ObjectId;}

		const D3DXVECTOR3* GetVelocity();
		void SetVelocity(D3DXVECTOR3 velocity);
		const float* GetRotationY();
		void SetRotationY(float rotationY);
	private:
		D3DXVECTOR3 Velocity;
		float RotationY;

		//Sorted array of components.
		ComponentArray Components;
		typedef ComponentArray::iterator ComponentIt;

		//A unique id for each object used to safely reference 
		//GOCs.
		GOCId ObjectId;

		//GAME_OBJECT_MANAGER ONLY
		GameObject();
		GameObject(std::ifstream* reader);
		//GAME_OBJECT_MANAGER ONLY: Use Destroy instead
		~GameObject();
	};

	//A Short name for GameObject
	typedef GameObject GOC;

	//A more advanced type safe way of accessing components.
	//Interface becomes Transform* transform = object->has(Transform);
	template<typename type>
	type * GameObject::GetComponetType(ComponentTypeId typeId)
	{
		return static_cast<type*>( GetComponent(typeId) );
	}

#define has(type) GetComponetType<type>( CT_##type )

}
